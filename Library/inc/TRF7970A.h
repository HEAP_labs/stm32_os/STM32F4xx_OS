/**
 * @file    ./Library/inc/TRF7970A.h
 * @author  Andreas Hirtenlehner
 * @brief   Header file for TRF7970A.c
 */

#ifndef __TRF7970A_H
#define __TRF7970A_H

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include "RTC.h"
#include "GPIO.h"
#include "SPI.h"
#include "errno.h"

/* Exported types ------------------------------------------------------------*/
typedef struct LIB_TRF7970A_config_s
{
  API_SPI_type_t*  API_SPIx;
  API_GPIO_type_t* mosi_portpin;
  API_GPIO_type_t* miso_portpin;
  API_GPIO_type_t* sck_portpin;
  API_GPIO_type_t* nss_portpin;
  API_GPIO_type_t* irq_portpin;
  API_GPIO_type_t* en_portpin;
  API_GPIO_type_t* en2_portpin;
  double           reading_period_s;
  uint16_t         SPI_BaudRatePrescaler_x;
} LIB_TRF7970A_config_t;

typedef struct LIB_TRF7970A_tag_data_s
{
  TM_RTC_Time_t timestamp;
  uint8_t       uid[10];
  uint8_t       uid_length;
  uint8_t       rssi_level;
  uint8_t       sak;
  uint16_t      atqa;
} LIB_TRF7970A_tag_data_t;

extern LIB_TRF7970A_tag_data_t LIB_TRF7970A_tag_data;

/* Exported constants --------------------------------------------------------*/
#define __RESET_RXDATA()  memset(RxData,  0, sizeof(RxData))
#define __RESET_MRXDATA() memset(mRxData, 0, sizeof(mRxData))
#define __RESET_TXDATA()  memset(TxData,  0, sizeof(TxData))

#ifndef RFID_COMMAND
  #define RFID_COMMAND  REQA
#endif

//Commands
#define GetSingleRegister   	((uint8_t)0x40)
#define GetMultiRegister 		  ((uint8_t)0x60)
#define SetSingleRegister 		((uint8_t)0x00)
#define SetMultiRegister 		  ((uint8_t)0x20)
#define SendCommand 			    ((uint8_t)0x80)

//ISO14443A commands
#define REQA					        ((uint8_t)0x26)
#define WUPA                  ((uint8_t)0x52)

//Register config
#define ChipStateControl			(vrs5_3 | agc_on | rf_on)
#define ISOControl						(iso_3 | rx_crc_n)
#define ModulatorControl			(Pm0 | Clo1 | Clo0)
#define RXSpecialSettings     (M848)
#define SFR10                 (col_7_6 | R4_bit_RX)
#define RXWaitTime_ISO14443AB (Rxw0 | Rxw1 | Rxw2)
#define IRQMask               (En_irq_col | En_irq_err3 | En_irq_err2 | En_irq_err1 | En_irq_fifo)

//Reader addresses
#define ChipStateControl_Address			((uint8_t)0x00)
#define ISOControl_Address						((uint8_t)0x01)
#define ISO14443Boptions_Address			((uint8_t)0x02)
#define ISO14443Aoptions_Address			((uint8_t)0x03)
#define TXtimerEPChigh_Address				((uint8_t)0x04)
#define TXtimerEPClow_Address					((uint8_t)0x05)
#define TXPulseLenghtControl_Address	((uint8_t)0x06)
#define RXNoResponseWaitTime_Address	((uint8_t)0x07)
#define RXWaitTime_Address						((uint8_t)0x08)
#define ModulatorControl_Address			((uint8_t)0x09)
#define RXSpecialSettings_Address			((uint8_t)0x0A)
#define RegulatorControl_Address			((uint8_t)0x0B)
#define IRQStatus_Address							((uint8_t)0x0C)
#define IRQMask_Address								((uint8_t)0x0D)
#define	CollisionPosition_Address			((uint8_t)0x0E)
#define RSSILevels_Address						((uint8_t)0x0F)
#define SFR10_Address				          ((uint8_t)0x10)
#define SFR11_Address				          ((uint8_t)0x11)
#define FIFOIRQLevels_Address         ((uint8_t)0x14)
#define NFCLowFieldLevel_Address      ((uint8_t)0x16)
#define NFCID_Address									((uint8_t)0x17)
#define NFCTargetLevel_Address				((uint8_t)0x18)
#define NFCTargetProtocol_Address			((uint8_t)0x19)
#define TestSetting1_Address					((uint8_t)0x1A)
#define TestSetting2_Address					((uint8_t)0x1B)
#define FIFOStatus_Address						((uint8_t)0x1C)
#define TXLenghtByte1_Address					((uint8_t)0x1D)
#define TXLenghtByte2_Address					((uint8_t)0x1E)
#define FIFO_Address									((uint8_t)0x1F)

//Reader commands
#define Idle									((uint8_t)0x00)
#define SoftInit							((uint8_t)0x03)
#define InitialRFCollision		((uint8_t)0x04)
#define ResponseRFCollisionN	((uint8_t)0x05)
#define ResponseRFCollision0	((uint8_t)0x06)
#define	Reset									((uint8_t)0x0F)
#define TransmitNoCRC					((uint8_t)0x10)
#define TransmitCRC						((uint8_t)0x11)
#define DelayTransmitNoCRC		((uint8_t)0x12)
#define DelayTransmitCRC			((uint8_t)0x13)
#define TransmitNextSlot			((uint8_t)0x14)
#define CloseSlotSequence			((uint8_t)0x15)
#define StopDecoders					((uint8_t)0x16)
#define RunDecoders						((uint8_t)0x17)
#define ChectInternalRF				((uint8_t)0x18)
#define CheckExternalRF				((uint8_t)0x19)
#define AdjustGain						((uint8_t)0x1A)

//Chip Status Control Register (ChipStateControl, 0x00, R/W, Default: 0x01)
#define vrs5_3								((uint8_t)0x01)
#define rec_on								((uint8_t)0x02)
#define agc_on								((uint8_t)0x04)
#define pm_on									((uint8_t)0x08)
#define rf_pwr								((uint8_t)0x10)
#define rf_on									((uint8_t)0x20)
#define direct								((uint8_t)0x40)
#define stby									((uint8_t)0x80)

//ISO Control Register (ISOControl, 0x01, R/W, Default: 0x02)
#define iso_0									((uint8_t)0x01)
#define iso_1									((uint8_t)0x02)
#define iso_2									((uint8_t)0x04)
#define iso_3									((uint8_t)0x08)
#define iso_4									((uint8_t)0x10)
#define rfid									((uint8_t)0x20)
#define dir_mode							((uint8_t)0x40)
#define rx_crc_n							((uint8_t)0x80)

//ISO14443B TX Options Register (ISO14443Boptions, 0x02, R/W, Default: 0x00)
//#define Unused							((uint8_t)0x01)
#define l_egt									((uint8_t)0x02)
#define sof_l0								((uint8_t)0x04)
#define sof_l1								((uint8_t)0x08)
#define eof_l0								((uint8_t)0x10)
#define egt0									((uint8_t)0x20)
#define egt1									((uint8_t)0x40)
#define egt2									((uint8_t)0x80)

//ISO14443A High-Bit-Rate and Parity Options Register (ISO14443Aoptions, 0x03, R/W, Default: 0x00)
//#define Unused							((uint8_t)0x01)
//#define Unused							((uint8_t)0x02)
//#define Unused							((uint8_t)0x04)
#define parity_2rx						((uint8_t)0x08)
#define parity_2tx						((uint8_t)0x10)
#define tx_br0								((uint8_t)0x20)
#define tx_br1								((uint8_t)0x40)
#define dif_tx_br							((uint8_t)0x80)

//TX Timer High Byte Control Register (TXtimerEPChigh, 0x04, R/W, Default: 0xC2)
#define tm_length8						((uint8_t)0x01)
#define tm_length9						((uint8_t)0x02)
#define tm_lengthA						((uint8_t)0x04)
#define tm_lengthB						((uint8_t)0x08)
#define tm_lengthC						((uint8_t)0x10)
#define tm_lengthD						((uint8_t)0x20)
#define tm_st0								((uint8_t)0x40)
#define tm_st1								((uint8_t)0x80)

//TX Timer Low Byte Control Register (TXtimerEPClow, 0x05, R/W, Default: 0x00)
#define tm_length0						((uint8_t)0x01)
#define tm_length1						((uint8_t)0x02)
#define tm_length2						((uint8_t)0x04)
#define tm_length3						((uint8_t)0x08)
#define tm_length4						((uint8_t)0x10)
#define tm_length5						((uint8_t)0x20)
#define tm_length6						((uint8_t)0x40)
#define tm_length7						((uint8_t)0x80)

//TX Pulse Length Control Register (TXPulseLenghtControl, 0x06, R/W, Default: 0x00)
#define Pul_c0								((uint8_t)0x01)
#define Pul_c1								((uint8_t)0x02)
#define Pul_c2								((uint8_t)0x04)
#define Pul_c3								((uint8_t)0x08)
#define Pul_c4								((uint8_t)0x10)
#define Pul_p0								((uint8_t)0x20)
#define Pul_p1								((uint8_t)0x40)
#define Pul_p2								((uint8_t)0x80)

//RX No Response Wait Time Register (RXNoResponseWaitTime, 0x07, R/W, Default: 0x0E)
#define NoResp0								((uint8_t)0x01)
#define NoResp1								((uint8_t)0x02)
#define NoResp2								((uint8_t)0x04)
#define NoResp3								((uint8_t)0x08)
#define NoResp4								((uint8_t)0x10)
#define NoResp5								((uint8_t)0x20)
#define NoResp6								((uint8_t)0x40)
#define NoResp7								((uint8_t)0x80)

//RX Wait Time Register (RXWaitTime, 0x08, R/W, Default: 0x1F)
#define Rxw0									((uint8_t)0x01)
#define Rxw1									((uint8_t)0x02)
#define Rxw2									((uint8_t)0x04)
#define Rxw3									((uint8_t)0x08)
#define Rxw4									((uint8_t)0x10)
#define Rxw5									((uint8_t)0x20)
#define Rxw6									((uint8_t)0x40)
#define Rxw7									((uint8_t)0x80)

//Modulator and SYS_CLK Control Register (ModulatorControl, 0x09, R/W, Default: 0x11)
#define Pm0										((uint8_t)0x01)
#define Pm1										((uint8_t)0x02)
#define Pm2										((uint8_t)0x04)
#define en_ana								((uint8_t)0x08)
#define Clo0									((uint8_t)0x10)
#define Clo1									((uint8_t)0x20)
#define en_ook_p							((uint8_t)0x40)
#define R27MHz						  	((uint8_t)0x80)

//RX Special Setting Register (RXSpecialSettings, 0x0A, R/W, Default: 0x40)
#define no_lim								((uint8_t)0x01)
#define agcr									((uint8_t)0x02)
#define gd2										((uint8_t)0x04)
#define gd1										((uint8_t)0x08)
#define hbt										((uint8_t)0x10)
#define M848									((uint8_t)0x20)
#define C424									((uint8_t)0x40)
#define C212									((uint8_t)0x80)

//Regulator and I/O Control Register (RegulatorControl, 0x0B, R/W, Default: 0x87)
#define vrs0									((uint8_t)0x01)
#define vrs1									((uint8_t)0x02)
#define vrs2									((uint8_t)0x04)
//#define Unused							((uint8_t)0x08)
//#define Unused							((uint8_t)0x10)
#define io_low								((uint8_t)0x20)
#define en_ext_pa							((uint8_t)0x40)
#define auto_reg							((uint8_t)0x80)

//IRQ Status Register (IRQStatus, 0x0C, R, Default: 0x00)
#define Irq_noresp			    	((uint8_t)0x01)
#define Irq_col								((uint8_t)0x02)
#define Irq_err3 							((uint8_t)0x04)
#define Irq_err2  						((uint8_t)0x08)
#define Irq_err1							((uint8_t)0x10)
#define Irq_fifo 							((uint8_t)0x20)
#define Irq_srx								((uint8_t)0x40)
#define Irq_tx								((uint8_t)0x80)

//Collision Position and Interrupt Mask Register (IRQMask, 0x0D, R/W, Default: 0x3E)
#define En_irq_noresp					((uint8_t)0x01)
#define En_irq_col						((uint8_t)0x02)
#define En_irq_err3						((uint8_t)0x04)
#define En_irq_err2						((uint8_t)0x08)
#define En_irq_err1						((uint8_t)0x10)
#define En_irq_fifo						((uint8_t)0x20)
#define Col8									((uint8_t)0x40)
#define Col9									((uint8_t)0x80)

//Collision Position Register (CollisionPosition, 0x0E, R, Default: 0x00)
#define Col0									((uint8_t)0x01)
#define Col1									((uint8_t)0x02)
#define Col2									((uint8_t)0x04)
#define Col3									((uint8_t)0x08)
#define Col4									((uint8_t)0x10)
#define Col5									((uint8_t)0x20)
#define Col6									((uint8_t)0x40)
#define Col7									((uint8_t)0x80)

//RSSI Levels and Oscillator Status Register (RSSILevels, 0x0F, R)
#define rssi_0								((uint8_t)0x01)
#define rssi_1								((uint8_t)0x02)
#define rssi_2								((uint8_t)0x04)
#define rssi_x0								((uint8_t)0x08)
#define rssi_x1								((uint8_t)0x10)
#define rssi_x2								((uint8_t)0x20)
#define osc_ok								((uint8_t)0x40)
//#define Unused							((uint8_t)0x80)

//Special Functions Register (SFR10, 0x10)
#define col_7_6               ((uint8_t)0x01)
#define R14_anticoll          ((uint8_t)0x02)
#define R4_bit_RX             ((uint8_t)0x04)
#define Sp_dir_mode           ((uint8_t)0x08)
#define next_slot_37us        ((uint8_t)0x10)
#define par43                 ((uint8_t)0x20)
//#define Unused							((uint8_t)0x40)
//#define Unused							((uint8_t)0x80)


//Test Register 1 (TestSetting1, 0x1A, R/W, Default: 0x00)
#define Test_AGC							((uint8_t)0x01)
#define zun										((uint8_t)0x02)
#define low1									((uint8_t)0x04)
#define low2									((uint8_t)0x80)
#define o_sel									((uint8_t)0x10)
#define MOD_Direct						((uint8_t)0x20)
#define MOD_Subc_Out					((uint8_t)0x40)
#define OOK_Subc_In						((uint8_t)0x80)

//Test Register 2 (TestSetting2, 0x1B, R/W, Default: 0x00)
#define clock_su							((uint8_t)0x01)
#define test_dec							((uint8_t)0x02)
#define test_io0							((uint8_t)0x04)
#define test_io1							((uint8_t)0x08)
//#define Unused							((uint8_t)0x10)
//#define Unused							((uint8_t)0x20)
//#define Unused							((uint8_t)0x40)
#define test_rf_level					((uint8_t)0x80)

//FIFO Status Register (FIFOStatus, 0x1C, R)
#define Fb0										((uint8_t)0x01)
#define Fb1										((uint8_t)0x02)
#define Fb2										((uint8_t)0x04)
#define Fb3										((uint8_t)0x08)
#define Fove									((uint8_t)0x10)
#define Flol									((uint8_t)0x20)
#define Fhil									((uint8_t)0x40)
#define RFU										((uint8_t)0x80)

//TX Length Byte1 Register (TXLenghtByte1, 0x1D, R/W, Default: 0x00)
#define Txl4									((uint8_t)0x01)
#define Txl5									((uint8_t)0x02)
#define Txl6									((uint8_t)0x04)
#define Txl7									((uint8_t)0x08)
#define Txl8									((uint8_t)0x10)
#define Txl9									((uint8_t)0x20)
#define Txl10									((uint8_t)0x40)
#define Txl11									((uint8_t)0x80)

//TX Length Byte2 Register (TXLenghtByte2, 0x1E, R/W, Default: 0x00)
#define Bbf										((uint8_t)0x01)
#define Bb0										((uint8_t)0x02)
#define Bb1										((uint8_t)0x04)
#define Bb2										((uint8_t)0x08)
#define Txl0									((uint8_t)0x10)
#define Txl1									((uint8_t)0x20)
#define Txl2									((uint8_t)0x40)
#define Txl3									((uint8_t)0x80)

#define MIFARE_MINI_ATQA        ((uint16_t)0x0004) /*SAK = 0x09, UID length 4 byte */
#define MIFARE_CLASSIC_1k_ATQA  ((uint16_t)0x0004) /*SAK = 0x08, UID length 4 byte */
#define MIFARE_CLASSIC_4k_ATQA  ((uint16_t)0x0002) /*SAK = 0x18, UID length 4 byte */
#define MIFARE_ULTRALIGHT_ATQA  ((uint16_t)0x0044) /*SAK = 0x00, UID length 7 byte */
#define MIFARE_DESFIRE_ATQA     ((uint16_t)0x0344) /*SAK = 0x20, UID length 7 byte */

#define MIFARE_MINI_SAK        ((uint8_t)0x09) /*SAK = 0x09, UID length 4 byte */
#define MIFARE_CLASSIC_1k_SAK  ((uint8_t)0x08) /*SAK = 0x08, UID length 4 byte */
#define MIFARE_CLASSIC_4k_SAK  ((uint8_t)0x18) /*SAK = 0x18, UID length 4 byte */
#define MIFARE_ULTRALIGHT_SAK  ((uint8_t)0x00) /*SAK = 0x00, UID length 7 byte */
#define MIFARE_DESFIRE_SAK     ((uint8_t)0x20) /*SAK = 0x20, UID length 7 byte */

#define CASCADE_LEVEL_1 ((uint8_t)0x93)
#define CASCADE_LEVEL_2 ((uint8_t)0x95)
#define CASCADE_LEVEL_3 ((uint8_t)0x97)
#define CT ((uint8_t)0x88) /* [Cascade Tag] byte (0x88), signals that the UID is not complete yet */

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void LIB_TRF7970A_lib_config(API_SPI_type_t* API_SPIx, API_GPIO_type_t* mosi_portpin, API_GPIO_type_t* miso_portpin, API_GPIO_type_t* sck_portpin, API_GPIO_type_t* nss_portpin, API_GPIO_type_t* irq_portpin, API_GPIO_type_t* en_portpin, API_GPIO_type_t* en2_portpin, double reading_period_s);
extern error_t LIB_TRF7970A_ISO14443A_statemachine(double TPeriod_s);
#endif // __TRF7970A_H
