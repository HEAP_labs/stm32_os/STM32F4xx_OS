/**
 * @file    ./Library/inc/Library.h
 * @author  Andreas Hirtenlehner
 * @brief   Header file to include Libraries defiened in LibUsings.h
 */

#ifndef __LIBRARY_H
#define __LIBRARY_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "LibUsings.h"

#ifdef __USING_DEBUG
  #include "Debug.h"
#endif // __USING_DEBUG

#ifdef __USING_HW_SIGNAL_READ
  #include "hw_signal_read.h"
#endif // __USING_LIB_HW_SIGNAL_READ

#ifdef __USING_TRF7970A
  #include "TRF7970A.h"
#endif // __USING_TRF7970A

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif // __LIBRARY_H
