/**
 * @file    ./Library/inc/LibUsings.h
 * @author  Andreas Hirtenlehner
 * @brief   Header file to define which Libraries are used
 */

#ifndef __LIBUSINGS_H
#define __LIBUSINGS_H

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
//#define __USING_DEBUG
//#define __USING_HW_SIGNAL_READ
//#define __USING_TRF7970A

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif // __LIBUSINGS_H
