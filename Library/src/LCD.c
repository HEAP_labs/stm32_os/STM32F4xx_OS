/* -----------------------------------------------------------------------------
   STM32F407_LCD.C
   -----------------------------------------------------------------------------
   Copyright:  (C) 2005 HTBLuVA Waidhofen an der Ybbs
   Standards:  Programmierrichtlinie Abteilung Elektrotechnik V1.2

   VERS: 1.0
   AUTH: Lf, Ge
   DATE: 22.12.2011

   Target: Sgs Thomson STM32F407

   Tool chain: KEIL uVision3 V4.22 Evaluation Version

   ---------------------- ZWECK ------------------------------------------------

   Vereinfachung der LCD Anteuerung.

   ---------------------- BESCHREIBUNG -----------------------------------------

   Dieses Objekt ist f�r ein 2 Zeilen x 16 Zeichen LCD-Display parametriert und
   steuert das Display im 4-Bit Modus an. Die LCD-Pins 4, 6, 14, 13, 12, und 11
   sind an den uC-Pins P1.0 bis P1.5 angeschlossen.

   Funktion zur LCD-Ansteuerung:
   =============================
   - lcd_start()
   - sprintf(lcd_zeile1, "   Hallo!    ")
   - sprintf(lcd_zeile2, "t = %5.2f ms ", f)

   Besondere Eigenschaften:
   ========================
   Dieses Objekt kann f�r jeden 8051'er Prozessor verwendet werden (nur lcd_start
   muss ggf. angepasst werden).

   Eingangsdaten:
   ==============
   Der Prozessortakt wird dem LCD bei Bedarf mittels define mitgeteilt

   Name     Defaultwert  Beschreibung
   takt_Hz  24500000/8   32000..100000000Hz (100MHz) Prozessortakt

   lcd_zeile1    Char-Array mit 16 Zeichen Text der ersten LCD-Zeile
   lcd_zeile2    Char-Array mit 16 Zeichen Text der zweiten LCD-Zeile

   Ausg�nge:
   =========
   Die Ausg�nge k�nnen mittels defines beliebigen bitadressierbaren Portpins
   zugeordnet werden.

   Ausgang Defaultpin Beschreibung
   oD7     P12        uC-Portpin f�r Datenleitung D7 (LCD-Pin 14)
   oD6     P13        uC-Portpin f�r Datenleitung D6 (LCD-Pin 13)
   oD5     P14        uC-Portpin f�r Datenleitung D5 (LCD-Pin 12)
   oD4     P15        uC-Portpin f�r Datenleitung D4 (LCD-Pin 11)
   oEN     P16        uC-Portpin f�r Enable Leitung (LCD-Pin 6)
   oRS     P17        uC-Portpin f�r Register Select Leitung (LCD-Pin 4)

   ---------------------- �NDERUNGEN -------------------------------------------
   30 AUG 2006 V0.1 Erste Version (ohne Initialisierung der static Variablen
                    und default Zweig der switch Anweisung um Code zu sparen)
   31 AUG 2006 V1.0 Initialisierung der static Variablen und default Zweig
                    bei der switch Anweisung erg�nzt.
   21 SEP 2006 V1.1 - Zerlegung in zwei Teilobjekte (text ausgeben und
                      zeichen ausgeben)
                    - Software Reset (zu Beginn 3 mal 0x30 ausgeben) implementiert
                    - Timing vereinfacht (Interruptfrequenz ist jetzt konstant)
                    - Zeit zwischen oEN = 1 und Daten anlegen war kleiner 1us
                      und betr�gt jetzt definiert einen Zyklus.
                    - Blinkender Cursor und Zeicheninvertierung ausschalten
   25 SEP 2006 V1.2 - Zusammenfassung der beiden Funktionsbl�cke Text und Byte
                      in die Timer ISR
	 22 DEZ 2011 V1.0	-Adaptierung f�r STM32F4-Prozessoren.
*/
#include "API.h"
#include "GPIO.h"

//#define lcd_tZyklus_100us 5   /* 100, 200, ... 500 us Aufrufintervall der LCD-Routinen */

#ifndef takt_Hz               /* ------ Prozessortakt ---------------------  */
#define takt_Hz (84000000) /* 32000..100000000Hz (100MHz) Prozessortakt   */
#endif                        /* ------ lcd Text --------------------------  */
#define iZeile1   lcd_zeile1  /* Name des char Arrays das f�r Text der ersten LCD-Zeile erzeugt wird */
#define iZeile2   lcd_zeile2  /* Name des char Arrays das f�r Text der zweiten LCD-Zeile erzeugt wird */

#ifndef oD7                   /* ----- Portpins ---------------------------  */
#define oD7       PG8         /* uC-Portpin f�r Datenleitung D7 (LCD-Pin 14) */
#define oD6       PG7         /* uC-Portpin f�r Datenleitung D6 (LCD-Pin 13) */
#define oD5       PG6         /* uC-Portpin f�r Datenleitung D5 (LCD-Pin 12) */
#define oD4       PG5         /* uC-Portpin f�r Datenleitung D4 (LCD-Pin 11) */
#define oEN       PD14         /* uC-Portpin f�r Enable Leitung (LCD-Pin 6)   */
#define oRS       PD12         /* uC-Portpin f�r Register Select Leitung (LCD-Pin 4) */
#endif                        /* ----- Funktionen -------------------------- */
#define start     lcd_start

/* -------------------------------------------------------------------------------
   Eingangsvariablen
   -------------------------------------------------------------------------------
   Der Inhalt dieser beiden Arrays wird automatisch am LCD ausgegeben.
*/
char     iZeile1[16]   = {"-  Enduro-TMS  -"}; /* Text der LCD-Zeile 1 */
char     iZeile2[20]   = {"- by  AH-GE-LP -"}; /* Text der LCD-Zeile 2 (4 Byte reserve, um Abst�rze zu vermeiden)*/


void LCD(float TZyklus){
   /* static Variablen (werden nur nach reset initialisiert) */
   static unsigned char text_oBef         =   0;
   static unsigned char text_oDat         =   0;
   static char          text_roAusgeben   =   0;
   static signed char   text_state        =   0;
   static unsigned char text_spalte       =   0;
   static  unsigned char text_delay_100us  = 250;
   const unsigned char  lcd_tZyklus_100us = TZyklus * 10000;

   static signed char   byte_state  = 2;

   /* -------------------------------------------------------------------------------
      lcd_text
      -------------------------------------------------------------------------------
      Dieser Block ermittelt den aktuell auszugebenden Befehl bzw. das auszugebende
      Zeichen. Nach der Initialisierung wird immer wenn ein Zeichen fertig ausgegeben
      ist (roAusgeben == 0), das n�chste auszugebende Zeichen ermittelt.
   */
   #define state         text_state       /* Zustand der LCD-Initialisierung und -Ausgabe */
   #define spalte        text_spalte      /* aktuelle Ausgabespalte */
   #define delay_100us   text_delay_100us /* delay nach Ausgabe des aktuellen Bytes. Einheit .. 100us*/
   #define oBef          text_oBef        /* Befehl (0 .. Datenausgabe, 1 .. Befehl, 2 .. Befehl - nur High-Nibble) */
   #define oDat          text_oDat        /* Auszugebendes Byte (Zeichen oder Befehl) */
   #define roAusgeben    text_roAusgeben  /* Handshake Signal f�r Ausgabe (1 ... Ausgeben, 0 ... Daten wurden ausgegeben) */

   /* count down bis zur Ausgabe des N�chsten Bytes */
   if(delay_100us > lcd_tZyklus_100us) delay_100us -= lcd_tZyklus_100us;
   else
   {
      /* N�chstes Byte ermitteln wenn count down abgelaufen und aktuelles Byte ausgegeben */
      if(!roAusgeben)
      {
         delay_100us = 250; /* Defaultwert */

         /* Das auszugebende Byte wird ausgew�hlt (bef und dat werden entsprechend gesetzt). */
         switch(state)
         {
            case 0:                                break; /* warten */
            case 1:                                       /* 4-Bit Mode - Vorbereiten */
            case 2:                                       /* 4-Bit Mode - Vorbereiten */
            case 3:  oBef = 2; oDat = 0x30;        break; /* 4-Bit Mode - Vorbereiten */
            case 4:            oDat = 0x20;        break; /* 4-Bit Mode         */
            case 5:  oBef = 1; oDat = 0x28;        break; /* Zweizeilig */
            case 6:            oDat = 0x0C;        break; /* kein blinkender Cursor */
            case 7:            oDat = 0x01;        break; /* Clear Display */
            case 8:  oBef = 1; oDat = 128 + 0x00;  spalte = 0; break;  /* setcursor auf Beginn 1.Zeile   */
            case 10: oBef = 1; oDat = 128 + 0x40;  spalte = 0; break;  /* setcursor auf Beginn 2.Zeile */
            case 9:  oBef = 0; oDat = iZeile1[spalte++]; delay_100us = 10;  if(spalte <= 15) state--; break; /* Zeile 1 ausgeben */
            case 11: oBef = 0; oDat = iZeile2[spalte++]; delay_100us = 10;  if(spalte <= 15) state--; else state = 7; break; /* Zeile 2 ausgeben */
            default: state = 0;
         }
         state++;
         roAusgeben = 1;
      }
   }

   #undef state
   #undef spalte
   #undef delay_100us
   #undef oBef
   #undef oDat
   #undef roAusgeben

   /* -------------------------------------------------------------------------------
      lcd_byte
      -------------------------------------------------------------------------------
      Dieser Block gibt den aktuellen Befehl bzw. das aktuelle Zeichen auf das LCD aus.
   */
   #define state        byte_state      /* Zustand innerhalb der Ausgabe eines Zeichens */
   #define iBef         text_oBef       /* Befehl (0 .. Datenausgabe, 1 .. Befehl, 2 .. Befehl - nur High-Nibble) */
   #define iDat         text_oDat       /* Auszugebendes Byte (Zeichen oder Befehl) */
   #define riAusgeben   text_roAusgeben /* Handshake Signal f�r Ausgabe (1 ... Ausgeben, 0 ... Daten wurden ausgegeben) */
   #define mDat                         /* Auszugebendes Byte */
   if(riAusgeben)
   {
      /* Ausgabe eines einzelnen Bytes in sieben Schritten (sieben Aufrufe der ISR) */
      /* state ==
         2 .. oEN auf 1 setzen
         3 .. High Nibble anlegen
         4 .. oEN auf 0 setzen
         5 .. -
         6 .. oEN auf 1 setzen
         7 .. Low Nibble anlegen
         8 .. oEN auf 0 setzen
      */
        if(state & 2) SetDO(oEN, Bit_SET);  else SetDO(oEN, Bit_RESET);


      if(state == 3) /* high-nibble anlegen */
      {
         if(iBef==0) SetDO(oRS, Bit_SET);      else SetDO(oRS, Bit_RESET);
         if(iDat & 16) SetDO(oD4, Bit_SET);  else SetDO(oD4, Bit_RESET);
         if(iDat & 32) SetDO(oD5, Bit_SET);  else SetDO(oD5, Bit_RESET);
         if(iDat & 64) SetDO(oD6, Bit_SET);  else SetDO(oD6, Bit_RESET);
         if(iDat & 128) SetDO(oD7, Bit_SET); else SetDO(oD7, Bit_RESET);
      }
      if(state == 7) /* low-nibble anlegen */
      {
         if(iDat & 1) SetDO(oD4, Bit_SET);  else SetDO(oD4, Bit_RESET);
         if(iDat & 2) SetDO(oD5, Bit_SET);  else SetDO(oD5, Bit_RESET);
         if(iDat & 4) SetDO(oD6, Bit_SET);  else SetDO(oD6, Bit_RESET);
         if(iDat & 8) SetDO(oD7, Bit_SET);  else SetDO(oD7, Bit_RESET);
      }

      if(state == 4 && iBef == 2) riAusgeben = 0; /* ausgeben abgeschlossen */
      if(state == 8)              riAusgeben = 0; /* ausgeben abgeschlossen */

      state++;
   }
   if(!riAusgeben) state = 2; /* "sicherer" Reset �ber Handshakesignal */

   #undef state
   #undef iBef
   #undef iDat
   #undef riAusgeben

}
#undef takt_Hz
#undef iZeile1
#undef iZeile2
#undef oPort
#undef oPortClk
#undef oD7
#undef oD6
#undef oD5
#undef oD4
#undef oEN
#undef oRS




