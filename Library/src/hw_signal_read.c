/**
 * @file    ./Library/src/hw_signal_read.c
 * @author  Andreas Hirtenlehner
 * @brief   reading a hardware digital input
 */

#include "LibUsings.h"
#ifdef __USING_HW_SIGNAL_READ

#include "hw_signal_read.h"

void LIB_hw_signal_read(LIB_hw_signal_t* hw_signal, double period_time_s);
uint8_t LIB_hw_signal_debounce(LIB_hw_signal_t* hw_signal, double period_time_s);
uint8_t LIB_hw_signal_pos_edge(LIB_hw_signal_t* hw_signal, double period_time_s);
uint8_t LIB_hw_signal_neg_edge(LIB_hw_signal_t* hw_signal, double period_time_s);
uint8_t LIB_hw_signal_edge(LIB_hw_signal_t* hw_signal, double period_time_s);

void LIB_hw_signal_read(LIB_hw_signal_t *hw_signal, double period_time_s)
{
    hw_signal->i_signal = API_getDI(hw_signal->portpin);

    if(hw_signal->t_on_s >= 1e8)  { /* limit */                         }
    else if(hw_signal->i_signal)  { hw_signal->t_on_s += period_time_s; }
    else                          { hw_signal->t_on_s = 0.0;            }

    if(hw_signal->t_off_s >= 1e8) { /* limit */                          }
    else if(!hw_signal->i_signal) { hw_signal->t_off_s += period_time_s; }
    else                          { hw_signal->t_off_s = 0.0;            }

    if(hw_signal->i_signal && hw_signal->t_on_s >= 0.005)        { hw_signal->o_debounced = 1;  }
    else if(!hw_signal->i_signal && hw_signal->t_off_s >= 0.005) { hw_signal->o_debounced = 0; }

    if(hw_signal->o_debounced && !hw_signal->m_o_debounced) { hw_signal->o_pos_edge = 1;  }
    else                                                              { hw_signal->o_pos_edge = 0; }

    if(!hw_signal->o_debounced && hw_signal->m_o_debounced) { hw_signal->o_neg_edge = 1;  }
    else                                                              { hw_signal->o_neg_edge = 0; }

    if(hw_signal->o_pos_edge)      { hw_signal->o_edge = 1;  }
    else if(hw_signal->o_neg_edge) { hw_signal->o_edge = 1;  }
    else                           { hw_signal->o_edge = 0; }

    hw_signal->m_o_debounced = hw_signal->o_debounced;
}

uint8_t LIB_hw_signal_debounce(LIB_hw_signal_t* hw_signal, double period_time_s)
{
    LIB_hw_signal_read(hw_signal, period_time_s);

    return hw_signal->o_debounced;
}


uint8_t LIB_hw_signal_pos_edge(LIB_hw_signal_t *hw_signal, double period_time_s)
{
    LIB_hw_signal_read(hw_signal, period_time_s);

    return hw_signal->o_pos_edge;
}

uint8_t LIB_hw_signal_neg_edge(LIB_hw_signal_t *hw_signal, double period_time_s)
{
    LIB_hw_signal_read(hw_signal, period_time_s);

    return hw_signal->o_neg_edge;
}

uint8_t LIB_hw_signal_edge(LIB_hw_signal_t *hw_signal, double period_time_s)
{
    LIB_hw_signal_read(hw_signal, period_time_s);

    return hw_signal->o_edge;
}

#endif // __USING_LIB_HW_SIGNAL_READ
