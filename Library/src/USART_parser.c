/**
  ******************************************************************************
  * @file    ./STM32Fx_OS/Library/src/Functiongenerator.c 
  * @author  EG, HA
  * @version V1.0.0
  * @date    18-November-2013
  * @brief   Parser function block
  ******************************************************************************
  * @RevisionHistory
	* 				 2013.11.18, V1.0.0, HA, -Datei Erstellung 
  *                                  -Funktionen USARTx_parser(), USART_CalcValue() und USART_WriteValue() hinzugefügt
  ******************************************************************************
  */
  
/* Includes ------------------------------------------------------------------*/
#include "Parser.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
char      CompareString[NumberOfValues][maxCompareStringLength];
uint32_t *USARTParserVal[NumberOfValues];

/* Private function prototypes -----------------------------------------------*/
void USART1_parser(uint8_t ActualByte);
void USART2_parser(uint8_t ActualByte);
void USART3_parser(uint8_t ActualByte);
void USART4_parser(uint8_t ActualByte);
void USART5_parser(uint8_t ActualByte);
void USART6_parser(uint8_t ActualByte);
double USART_CalcValue(uint8_t *USART_RxValueBuffer, uint8_t KommaPosition);
void USART_WriteValue(char *USART_RxCompareStringBuffer, double Wert);
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  calculates the received value and write it to the selected memory address
  * @param  ActualByte: Received Byte from USART
  * @retval none
  */
void USART1_parser(uint8_t ActualByte){
  static uint8_t ActualPosition   = 0;
  static uint8_t KommaPosition    = 0;
  static uint8_t i                = 0;
  static uint8_t Z                = 0;
  static double  Value            = 0;
  static char USART_RxCompareStringBuffer[maxCompareStringLength];
  static uint8_t USART_RxValueBuffer[maxValueLength];
  
  if(Z == 0 && ActualPosition > maxCompareStringLength)     { ActualPosition = 0;                                                         }
  else if(Z == 0 && ActualByte == delimiter_char)           { Z = 1;                                                                      }                                                         
  else if(Z == 0 && ActualByte != ' ')                      { USART_RxCompareStringBuffer[ActualPosition] = ActualByte; ActualPosition++; }
  
  if(Z == 1 && ActualPosition > maxValueLength)                       { Z = 0; ActualPosition = 0;                                          }
  else if(Z == 1 && ActualByte == delimiter_char)                     { ActualPosition = 0;                                                 }                                                         
  else if(Z == 1 && ActualByte == '.')                                { KommaPosition = ActualPosition;                                     }
  else if(Z == 1 && ActualByte == ',')                                { KommaPosition = ActualPosition;                                     }
  else if(Z == 1 && ActualByte == closing_char && KommaPosition == 0) { KommaPosition = ActualPosition; Z = 2;                              } 
  else if(Z == 1 && ActualByte == closing_char)                       { Z = 2;                                                              } 
  else if(Z == 1 && ActualByte != ' ')                                { USART_RxValueBuffer[ActualPosition] = ActualByte; ActualPosition++; }

  if(Z == 2){ 
    Value = USART_CalcValue(USART_RxValueBuffer, KommaPosition); 
    USART_WriteValue(USART_RxCompareStringBuffer, Value); 

    for(i = 0; i < maxCompareStringLength; i++) USART_RxCompareStringBuffer[i] = 0;
    for(i = 0; i < maxValueLength; i++)         USART_RxValueBuffer[i]          = 0;

    KommaPosition  = 0;
    ActualPosition = 0;
    Z              = 0;
  } 
}

/**
  * @brief  calculates the received value and write it to the selected memory address
  * @param  ActualByte: Received Byte from USART
  * @retval none
  */
void USART2_parser(uint8_t ActualByte){
  static uint8_t ActualPosition   = 0;
  static uint8_t KommaPosition    = 0;
  static uint8_t i                = 0;
  static uint8_t Z                = 0;
  static double  Value            = 0;
  static char USART_RxCompareStringBuffer[maxCompareStringLength];
  static uint8_t USART_RxValueBuffer[maxValueLength];
  
  if(Z == 0 && ActualPosition > maxCompareStringLength) { ActualPosition = 0;                                                         }
  else if(Z == 0 && ActualByte == delimiter_char)           { Z = 1;                                                                      }                                                         
  else if(Z == 0 && ActualByte != ' ')                      { USART_RxCompareStringBuffer[ActualPosition] = ActualByte; ActualPosition++; }
  
  if(Z == 1 && ActualPosition > maxValueLength)                   { Z = 0; ActualPosition = 0;                                          }
  else if(Z == 1 && ActualByte == delimiter_char)                     { ActualPosition = 0;                                                 }                                                         
  else if(Z == 1 && ActualByte == '.')                                { KommaPosition = ActualPosition;                                     }
  else if(Z == 1 && ActualByte == ',')                                { KommaPosition = ActualPosition;                                     }
  else if(Z == 1 && ActualByte == closing_char && KommaPosition == 0) { KommaPosition = ActualPosition; Z = 2;                              } 
  else if(Z == 1 && ActualByte == closing_char)                       { Z = 2;                                                              } 
  else if(Z == 1 && ActualByte != ' ')                                { USART_RxValueBuffer[ActualPosition] = ActualByte; ActualPosition++; }

  if(Z == 2){ 
    Value = USART_CalcValue(USART_RxValueBuffer, KommaPosition); 
    USART_WriteValue(USART_RxCompareStringBuffer, Value); 

    for(i = 0; i < maxCompareStringLength; i++) USART_RxCompareStringBuffer[i] = 0;
    for(i = 0; i < maxValueLength; i++)         USART_RxValueBuffer[i]          = 0;

    KommaPosition  = 0;
    ActualPosition = 0;
    Z              = 0;
  } 
}

/**
  * @brief  calculates the received value and write it to the selected memory address
  * @param  ActualByte: Received Byte from USART
  * @retval none
  */
void USART3_parser(uint8_t ActualByte){
  static uint8_t ActualPosition   = 0;
  static uint8_t KommaPosition    = 0;
  static uint8_t i                = 0;
  static uint8_t Z                = 0;
  static double  Value            = 0;
  static char USART_RxCompareStringBuffer[maxCompareStringLength];
  static uint8_t USART_RxValueBuffer[maxValueLength];
  
  if(Z == 0 && ActualPosition > maxCompareStringLength) { ActualPosition = 0;                                                         }
  else if(Z == 0 && ActualByte == delimiter_char)           { Z = 1;                                                                      }                                                         
  else if(Z == 0 && ActualByte != ' ')                      { USART_RxCompareStringBuffer[ActualPosition] = ActualByte; ActualPosition++; }
  
  if(Z == 1 && ActualPosition > maxValueLength)                   { Z = 0; ActualPosition = 0;                                          }
  else if(Z == 1 && ActualByte == delimiter_char)                     { ActualPosition = 0;                                                 }                                                         
  else if(Z == 1 && ActualByte == '.')                                { KommaPosition = ActualPosition;                                     }
  else if(Z == 1 && ActualByte == ',')                                { KommaPosition = ActualPosition;                                     }
  else if(Z == 1 && ActualByte == closing_char && KommaPosition == 0) { KommaPosition = ActualPosition; Z = 2;                              } 
  else if(Z == 1 && ActualByte == closing_char)                       { Z = 2;                                                              } 
  else if(Z == 1 && ActualByte != ' ')                                { USART_RxValueBuffer[ActualPosition] = ActualByte; ActualPosition++; }

  if(Z == 2){ 
    Value = USART_CalcValue(USART_RxValueBuffer, KommaPosition); 
    USART_WriteValue(USART_RxCompareStringBuffer, Value); 

    for(i = 0; i < maxCompareStringLength; i++) USART_RxCompareStringBuffer[i] = 0;
    for(i = 0; i < maxValueLength; i++)         USART_RxValueBuffer[i]          = 0;

    KommaPosition  = 0;
    ActualPosition = 0;
    Z              = 0;
  } 
}

/**
  * @brief  calculates the received value and write it to the selected memory address
  * @param  ActualByte: Received Byte from USART
  * @retval none
  */
void USART4_parser(uint8_t ActualByte){
  static uint8_t ActualPosition   = 0;
  static uint8_t KommaPosition    = 0;
  static uint8_t i                = 0;
  static uint8_t Z                = 0;
  static double  Value            = 0;
  static char USART_RxCompareStringBuffer[maxCompareStringLength];
  static uint8_t USART_RxValueBuffer[maxValueLength];
  
  if(Z == 0 && ActualPosition > maxCompareStringLength) { ActualPosition = 0;                                                         }
  else if(Z == 0 && ActualByte == delimiter_char)           { Z = 1;                                                                      }                                                         
  else if(Z == 0 && ActualByte != ' ')                      { USART_RxCompareStringBuffer[ActualPosition] = ActualByte; ActualPosition++; }
  
  if(Z == 1 && ActualPosition > maxValueLength)                   { Z = 0; ActualPosition = 0;                                          }
  else if(Z == 1 && ActualByte == delimiter_char)                     { ActualPosition = 0;                                                 }                                                         
  else if(Z == 1 && ActualByte == '.')                                { KommaPosition = ActualPosition;                                     }
  else if(Z == 1 && ActualByte == ',')                                { KommaPosition = ActualPosition;                                     }
  else if(Z == 1 && ActualByte == closing_char && KommaPosition == 0) { KommaPosition = ActualPosition; Z = 2;                              } 
  else if(Z == 1 && ActualByte == closing_char)                       { Z = 2;                                                              } 
  else if(Z == 1 && ActualByte != ' ')                                { USART_RxValueBuffer[ActualPosition] = ActualByte; ActualPosition++; }

  if(Z == 2){ 
    Value = USART_CalcValue(USART_RxValueBuffer, KommaPosition); 
    USART_WriteValue(USART_RxCompareStringBuffer, Value); 

    for(i = 0; i < maxCompareStringLength; i++) USART_RxCompareStringBuffer[i] = 0;
    for(i = 0; i < maxValueLength; i++)         USART_RxValueBuffer[i]          = 0;

    KommaPosition  = 0;
    ActualPosition = 0;
    Z              = 0;
  } 
}

/**
  * @brief  calculates the received value and write it to the selected memory address
  * @param  ActualByte: Received Byte from USART
  * @retval none
  */
void USART5_parser(uint8_t ActualByte){
  static uint8_t ActualPosition   = 0;
  static uint8_t KommaPosition    = 0;
  static uint8_t i                = 0;
  static uint8_t Z                = 0;
  static double  Value            = 0;
  static char USART_RxCompareStringBuffer[maxCompareStringLength];
  static uint8_t USART_RxValueBuffer[maxValueLength];
  
  if(Z == 0 && ActualPosition > maxCompareStringLength) { ActualPosition = 0;                                                         }
  else if(Z == 0 && ActualByte == delimiter_char)           { Z = 1;                                                                      }                                                         
  else if(Z == 0 && ActualByte != ' ')                      { USART_RxCompareStringBuffer[ActualPosition] = ActualByte; ActualPosition++; }
  
  if(Z == 1 && ActualPosition > maxValueLength)                   { Z = 0; ActualPosition = 0;                                          }
  else if(Z == 1 && ActualByte == delimiter_char)                     { ActualPosition = 0;                                                 }                                                         
  else if(Z == 1 && ActualByte == '.')                                { KommaPosition = ActualPosition;                                     }
  else if(Z == 1 && ActualByte == ',')                                { KommaPosition = ActualPosition;                                     }
  else if(Z == 1 && ActualByte == closing_char && KommaPosition == 0) { KommaPosition = ActualPosition; Z = 2;                              } 
  else if(Z == 1 && ActualByte == closing_char)                       { Z = 2;                                                              } 
  else if(Z == 1 && ActualByte != ' ')                                { USART_RxValueBuffer[ActualPosition] = ActualByte; ActualPosition++; }

  if(Z == 2){ 
    Value = USART_CalcValue(USART_RxValueBuffer, KommaPosition); 
    USART_WriteValue(USART_RxCompareStringBuffer, Value); 

    for(i = 0; i < maxCompareStringLength; i++) USART_RxCompareStringBuffer[i] = 0;
    for(i = 0; i < maxValueLength; i++)         USART_RxValueBuffer[i]          = 0;

    KommaPosition  = 0;
    ActualPosition = 0;
    Z              = 0;
  } 
}

/**
  * @brief  calculates the received value and write it to the selected memory address
  * @param  ActualByte: Received Byte from USART
  * @retval none
  */
void USART6_parser(uint8_t ActualByte){
  static uint8_t ActualPosition   = 0;
  static uint8_t KommaPosition    = 0;
  static uint8_t i                = 0;
  static uint8_t Z                = 0;
  static double  Value            = 0;
  static char USART_RxCompareStringBuffer[maxCompareStringLength];
  static uint8_t USART_RxValueBuffer[maxValueLength];
  
  if(Z == 0 && ActualPosition > maxCompareStringLength) { ActualPosition = 0;                                                         }
  else if(Z == 0 && ActualByte == delimiter_char)           { Z = 1;                                                                      }                                                         
  else if(Z == 0 && ActualByte != ' ')                      { USART_RxCompareStringBuffer[ActualPosition] = ActualByte; ActualPosition++; }
  
  if(Z == 1 && ActualPosition > maxValueLength)                   { Z = 0; ActualPosition = 0;                                          }
  else if(Z == 1 && ActualByte == delimiter_char)                     { ActualPosition = 0;                                                 }                                                         
  else if(Z == 1 && ActualByte == '.')                                { KommaPosition = ActualPosition;                                     }
  else if(Z == 1 && ActualByte == ',')                                { KommaPosition = ActualPosition;                                     }
  else if(Z == 1 && ActualByte == closing_char && KommaPosition == 0) { KommaPosition = ActualPosition; Z = 2;                              } 
  else if(Z == 1 && ActualByte == closing_char)                       { Z = 2;                                                              } 
  else if(Z == 1 && ActualByte != ' ')                                { USART_RxValueBuffer[ActualPosition] = ActualByte; ActualPosition++; }

  if(Z == 2){ 
    Value = USART_CalcValue(USART_RxValueBuffer, KommaPosition); 
    USART_WriteValue(USART_RxCompareStringBuffer, Value); 

    for(i = 0; i < maxCompareStringLength; i++) USART_RxCompareStringBuffer[i] = 0;
    for(i = 0; i < maxValueLength; i++)         USART_RxValueBuffer[i]          = 0;

    KommaPosition  = 0;
    ActualPosition = 0;
    Z              = 0;
  } 
}

/**
  * @brief  calculates the received value
  * @param  USART_RxValueBuffer: Received byte array from USART_x
  * @retval Value: received value
  */
double USART_CalcValue(uint8_t *USART_RxValueBuffer, uint8_t KommaPosition) {
  double Value = 0;

  if(KommaPosition - 6 > 0) Value += (USART_RxValueBuffer[KommaPosition - 6] & 0x0F) *    1000000.0;
  if(KommaPosition - 5 > 0) Value += (USART_RxValueBuffer[KommaPosition - 6] & 0x0F) *     100000.0;
  if(KommaPosition - 4 > 0) Value += (USART_RxValueBuffer[KommaPosition - 5] & 0x0F) *      10000.0;
  if(KommaPosition - 3 > 0) Value += (USART_RxValueBuffer[KommaPosition - 4] & 0x0F) *       1000.0;
  if(KommaPosition - 2 > 0) Value += (USART_RxValueBuffer[KommaPosition - 3] & 0x0F) *        100.0;
  if(KommaPosition - 1 > 0) Value += (USART_RxValueBuffer[KommaPosition - 2] & 0x0F) *         10.0;
                            Value += (USART_RxValueBuffer[KommaPosition - 1] & 0x0F) *          1.0;
                            Value += (USART_RxValueBuffer[KommaPosition + 0] & 0x0F) *          0.1;
                            Value += (USART_RxValueBuffer[KommaPosition + 1] & 0x0F) *         0.01;
                            Value += (USART_RxValueBuffer[KommaPosition + 2] & 0x0F) *        0.001;
                            Value += (USART_RxValueBuffer[KommaPosition + 3] & 0x0F) *       0.0001;
                            Value += (USART_RxValueBuffer[KommaPosition + 4] & 0x0F) *      0.00001;
                            Value += (USART_RxValueBuffer[KommaPosition + 5] & 0x0F) *     0.000001; 
                            Value += (USART_RxValueBuffer[KommaPosition + 6] & 0x0F) *    0.0000001;
                            Value += (USART_RxValueBuffer[KommaPosition + 7] & 0x0F) *   0.00000001;
                            Value += (USART_RxValueBuffer[KommaPosition + 8] & 0x0F) *  0.000000001;

  return Value;
}

/**
  * @brief  writes the received value to the selected memory address
  * @param  USART_RxCompareStringBuffer[]: Received compare string from USART_x
  * @retval none
  */
void USART_WriteValue(char *USART_RxCompareStringBuffer, double Value) {
  uint8_t i = 0;
  
  for(i = 0; i <= NumberOfValues; i++){
    if(strncmp(USART_RxCompareStringBuffer, CompareString[i], maxCompareStringLength) == 0) { *USARTParserVal[i] = Value; } 
  }
}
