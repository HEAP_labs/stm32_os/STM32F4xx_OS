#include "USART.h"
#include "XBee868pro.h"
#include <stdlib.h>





void initXBeeTxFrame(XBee_Tx_Frame_t *f){
  f->startdelimiter = START_DELIMITER;
  f->length = 14;
  f->type = TRANSMIT_REQUEST;
  f->id = 0x00;
  // broadcast addr
  f->destaddr[0] = 0xFF;
  f->destaddr[1] = 0xFF;
  f->destaddr[2] = 0;
  f->destaddr[3] = 0;
  f->destaddr[4] = 0;
  f->destaddr[5] = 0;
  f->destaddr[6] = 0;
  f->destaddr[7] = 0;
  f->reserved = 0xFFFE;
  f->broadcastradius = 0;
  f->transmitoptions = 0;
}


uint8_t getchecksum(XBee_Tx_Frame_t *f){
  uint8_t cs = 0;
  uint16_t i;
  uint16_t len = f->length - 14;

  cs += f->type;
  cs += f->id;
  cs += f->destaddr[0];
  cs += f->destaddr[1];
  cs += f->destaddr[2];
  cs += f->destaddr[3];
  cs += f->destaddr[4];
  cs += f->destaddr[5];
  cs += f->destaddr[6];
  cs += f->destaddr[7];
  cs += f->reserved & 0xFF;
  cs += f->reserved & 0xFF00 >> 8;
  cs += f->broadcastradius;
  cs += f->transmitoptions;

  for(i = 0; i<len;i++){
    cs += f->rfdata[i];
  }

  return cs;
}


void XBee_send(char *data, uint8_t dest_addr[8])
{
  XBee_Tx_Frame_t f;

  initXBeeTxFrame(f);
  memcpy(f->destaddr, dest_addr, 8);
  f->rfdata = data;
  f->checksum = getchecksum();

  // send header
    USARTSendData(USART_XBEE, USART_XBEE_TxPin, USART_XBEE_BAUDRATE, &f, XBEE_TX_HEADER_LEN);
    // send data
    USARTSendData(USART_XBEE, USART_XBEE_TxPin, USART_XBEE_BAUDRATE, f->rfdata, strlen(f->rfdata)+1);
    // send checksum
  USARTSendData(USART_XBEE, USART_XBEE_TxPin, USART_XBEE_BAUDRATE, &(f->checksum), 1);
}

uint8_t* packFrame(uint8_t *data, uint16_t length_byte, uint8_t dest_addr[8])
{

}

void XBeeInit()
{

}

uint8_t* getUID()
{

}

