#ifndef DEVICEDRIVER_H_INCLUDED
#define DEVICEDRIVER_H_INCLUDED


#define USE_VCP 1

long com3_write( int fd, const char *ptr, int len );
long com3_read(int fd, char *ptr, int len);
long com3_close(int fd);
long com3_open(const char *path, int flags, int mode);

long vcp_write( int fd, const char *ptr, int len );
long vcp_open(const char *path, int flags, int mode);
long vcp_close(int fd);
long vcp_read(int fd, char *ptr, int len);


#endif /* DEVICEDRIVER_H_INCLUDED */
