/**
 ******************************************************************************
 * @file      semihosting.c
 * @author    Coocox
 * @version   V1.0
 * @date      09/10/2011
 * @brief     Semihosting LowLayer GetChar/SendChar Implement.
 *
 *******************************************************************************
 */

//#include <sys/types.h>
//#include <sys/stat.h>
#include "semihosting.h"
#include "stm32f4xx.h"

static char g_buf[16];
static char g_buf_len = 0;

/**************************************************************************//**
 * @brief  Determines if processor is in debug mode
 *
 * @return 1..Processor in debug mode 0..Processor in normal mode
 *****************************************************************************/
long SH_IsDebugMode(){
	return (CoreDebug->DHCSR & 0x00000001)==0x01;
}

/**************************************************************************//**
 * @brief  Transmit data on semihosting mode.
 *
 * @param  ptr is the start address of data to be sent
 *
 * @param  len is number of bytes to send
 *
 * @return  0..on success 1..on error
 *****************************************************************************/
long SH_SendData(char* ptr, long len){
	long i;
	for(i=0;i<len;i++)
		SH_SendChar(*(ptr+i));
    return 0; //success
}

/**************************************************************************//**
 * @brief  Receive data on semihosting mode.
 *
 * @param  ptr is the start address of buffer
 *
 * @param  len is number of bytes to read
 *
 * @return
 *****************************************************************************/
long SH_RecvData(char* ptr, long len){
	long i;
	for(i=0;i<len;i++)
		ptr[i] = SH_GetChar();
  return 1; //success
}


/**************************************************************************//**
 * @brief  Transmit a char on semihosting mode.
 *
 * @param  ch is the char that to send.
 *
 * @return Character to write.
 *****************************************************************************/
void SH_SendChar(int ch) {
	g_buf[g_buf_len++] = ch;
	g_buf[g_buf_len] = '\0';
	if (g_buf_len + 1 >= sizeof(g_buf) || ch == '\n' || ch == '\0') {
		g_buf_len = 0;
		/* Send the char */
		if (SH_DoCommand(0x04, (int) g_buf, 0) != 0) {
			return;
		}
	}
}

/**************************************************************************//**
 * @brief  Transmit a null-terminated string on semihosting mode.
 *
 * @param  str is the string that to send.
 *
 * @return Character to write.
 *****************************************************************************/
void SH_SendString(const char *str)
{
	if (SH_DoCommand(0x04, (int)str, 0) != 0) {
		return;
	}
}

/**************************************************************************//**
 * @brief  Read a char on semihosting mode.
 *
 * @param  None.
 *
 * @return Character that have read.
 *****************************************************************************/
char SH_GetChar() {
	int nRet;

	while (SH_DoCommand(0x101, 0, &nRet) != 0) {
		if (nRet != 0) {
			SH_DoCommand(0x07, 0, &nRet);
			return (char) nRet;
		}
	}
	return 0;
}

