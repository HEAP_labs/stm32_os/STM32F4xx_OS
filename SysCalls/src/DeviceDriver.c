
#include "USART.h"
#include "Debug.h"
#include<stdbool.h>

/*#include "usbd_cdc_core.h"
#include "usbd_usr.h"
#include "usb_conf.h"
#include "usbd_desc.h"
#include "usbd_cdc_vcp.h"*/
#include "stdio.h"

/* Kommunikationseinstellungen
 * USART:
 */
#define COM3 	    USART_3
#define TxPin	 	  PD8
#define RxPin     PD9
#define BAUDRATE	115200

void init_usart3_dma(void);

long com3_write( int fd, const char *ptr, int len )
{
    static bool iniOK = false;

    // do a dummy write for proper initialization
    if(!iniOK)
    {
        USARTSendData(COM3, TxPin, BAUDRATE, ptr, 1);
    }

    iniOK = true;
    USARTSendData(COM3, TxPin, BAUDRATE, ptr, len);
    return len;
}


long com3_open(const char *path, int flags, int mode)
{
    return 0;   // STDOUT always open
}


long com3_close(int fd)
{
    return -1; //STDOUT can't be closed
}

long com3_read(int fd, char *ptr, int len)
{
    static bool iniOK = false;

    if(!iniOK)
        USARTReceive(COM3, RxPin, BAUDRATE); // init
    iniOK = true;

    while(USART_GetFlagStatus(COM3.USARTx, USART_FLAG_RXNE)==RESET);
    *ptr = USARTReceive(COM3, RxPin, BAUDRATE);

    return 1; //not yet implemented
}


/** Hilfsfunktionen **/

/*User variables -------------------------------------------------------------*/
#define RXBUFFERSIZE 4
uint8_t RxBuffer[RXBUFFERSIZE];


/* Private functions ---------------------------------------------------------*/
//initialization of USART3 used with DMA
void init_usart3_dma(void)
{
//  GPIO_InitTypeDef GPIO_InitStruct;
    NVIC_InitTypeDef NVIC_InitStructure;
//  USART_InitTypeDef USART_InitStruct;
    DMA_InitTypeDef DMA_InitStructure;

//  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE); //USART3 Clock Enable
//  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE); //GPIOB Clock Enable
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE); //DMA1 Clock Enable

//  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11;
//  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
//  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
//  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
//  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
//  GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_USART3); //USART3 Tx Pin
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_USART3); //USART3 Rx Pin

    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

    // Enable the USART3 RX DMA Interrupt
    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Stream1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 13;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

//  USART_InitStruct.USART_BaudRate = 19200;
//  USART_InitStruct.USART_WordLength = USART_WordLength_8b;
//  USART_InitStruct.USART_StopBits = USART_StopBits_1;
//  USART_InitStruct.USART_Parity = USART_Parity_No;
//  USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
//  USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
//
//  USART_Init(USART3, &USART_InitStruct);

//  USART_Cmd(USART3, ENABLE); // Enable USART3


    // ----- USART3 DMA Rx Stream Init -----
    DMA_DeInit(DMA1_Stream1);

    DMA_InitStructure.DMA_Channel = DMA_Channel_4;
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&USART3->DR;
    DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)RxBuffer;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory; // Receive From Per to Mem
    DMA_InitStructure.DMA_BufferSize = RXBUFFERSIZE;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
    DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Enable; // ??????
    DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
    DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
    DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;

    DMA_Init(DMA1_Stream1, &DMA_InitStructure);

    USART_DMACmd(USART3, USART_DMAReq_Rx, ENABLE); // Enable USART Rx DMA Request
    DMA_ITConfig(DMA1_Stream1, DMA_IT_TC, ENABLE); // Enable Transfer Complete Interrupt
    DMA_Cmd(DMA1_Stream1, ENABLE); // Enable DMA Rx Stream
}

void DMA1_Stream1_IRQHandler(void) // USART3 RX DMA Transfer Complete Interrupt
{
    if (DMA_GetITStatus(DMA1_Stream1, DMA_IT_TCIF1))
    {
        // ----------

        DMA_ClearITPendingBit(DMA1_Stream1, DMA_IT_TCIF1);
        printf("USART DEMA finished\n");
    }
}
/********************/


/* Kommunikationseinstellungen
 * VCP:
 */

void vcp_init()
{
    static bool iniOK = false;
    if(!iniOK)
    {
#if USE_VCP
        USBD_Init(&USB_OTG_dev,
#ifdef USE_USB_OTG_HS
                  USB_OTG_HS_CORE_ID,
#else
                  USB_OTG_FS_CORE_ID,
#endif
                  &USR_desc,
                  &USBD_CDC_cb,
                  &USR_cb);
#endif
    }
    iniOK = true;
}

long vcp_write( int fd, const char *ptr, int len )
{

#if USE_VCP
    vcp_init();
    VCP_DataTx(ptr,len);
    return len;
#else
    return -1;
#endif
}


long vcp_open(const char *path, int flags, int mode)
{
    return 0;   // STDOUT always open
}


long vcp_close(int fd)
{
    return -1; //STDOUT can't be closed
}

long vcp_read(int fd, char *ptr, int len)
{
#if USE_VCP
    vcp_init();
    VCP_DataRx(ptr,len);
    return len;
#else
    return -1;
#endif
}



