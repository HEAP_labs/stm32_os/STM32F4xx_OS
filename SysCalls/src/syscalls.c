/**
********************************************************************************
* @file     syscalls.c
* @author   Gerald Ebmer
* @version  V1.1.0
* @date     02-April-2015
* @brief    Implementation of newlib syscall
*           reentrant functions will be mapped to non-reentrant (newlib)
*  @verbatim
*     Version History:
*       V1.0.0  xx.xx.xxxx  GE  Syscalls and device driver support
*       V1.1.0  02.04.2015  GE  FatFs support included
*
*     Todo:
*       - Semihosting support
*       - VCP support
*  @endverbatim
*
********************************************************************************
*/


/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdarg.h>
//#include <sys/types.h>
//#include <sys/stat.h>
#include <string.h>
#include <errno.h>
//#include <sys/times.h>
#include "Debug.h"
#include "semihosting.h"
//#include "usbd_cdc_vcp.h"
#include "DeviceDriver.h"
//#include "tm_stm32f4_delay.h"
//#include "tm_stm32f4_fatfs.h"
//

#include "defines.h"

/** @addtogroup C runtime
  * @{
  */

/** @defgroup SYSCALLS
  * @brief Syscall implementations
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
typedef struct devoptab_s
{
    const char *name;
    long (*open )  ( const char *path, int flags, int mode );
    long (*close ) ( int fd );
    long (*write ) ( int fd, const char *ptr, int len );
    long (*read )  ( int fd, char *ptr, int len );
} devoptab_t;

/* Private define ------------------------------------------------------------*/
//#undef errno
#define MAX_OPEN_FILES 50

/* Private macro -------------------------------------------------------------*/
#define FATFS_STARTINDEX 4//sizeof(devoptab_list) // startindex for FatFs-files

#define SYSCALL_DEBUG_INFO      (SYSCALL_DEBUG | SYSTEM_DBG_LEVEL_ALL)
#define SYSCALL_DEBUG_WARNING   (SYSCALL_DEBUG | SYSTEM_DBG_LEVEL_WARNING)
#define SYSCALL_DEBUG_SERIOUS   (SYSCALL_DEBUG | SYSTEM_DBG_LEVEL_SERIOUS)
#define SYSCALL_DEBUG_SEVERE    (SYSCALL_DEBUG | SYSTEM_DBG_LEVEL_SEVERE)

/* Private variables ---------------------------------------------------------*/
//extern int errno;
extern int  __HeapLimit;
FATFS FatFs; // Fatfs object
FIL Files[MAX_OPEN_FILES]; // File object
char IsOpenFile[MAX_OPEN_FILES] = { 0 };

/* standard stream device */
const devoptab_t devoptab_com3 = { "COM3", com3_open, com3_close, com3_write, com3_read };
const devoptab_t devoptab_vcp  = { "VCP", vcp_open, vcp_close, vcp_write, vcp_read };
/* Device Operations Table */
const devoptab_t *devoptab_list[] =
{
    &devoptab_com3, /* standard input */
    &devoptab_com3, /* standard output */
    &devoptab_com3, /* standard error */
    &devoptab_com3, /* another device */
    /* and so on... */
    0 /* terminates the list */
};

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  changes the amount of space allocated for the calling process's
  *         data segment
  * @param  *reent
  * @param  number of bytes to allocate
  * @retval address of data segment
  */
__attribute__ ((used))
caddr_t _sbrk_r ( struct _reent *reent, int incr )
{
    static unsigned char *heap = NULL;
    unsigned char *prev_heap;

    if (heap == NULL)
    {
        heap = (unsigned char *)&__HeapLimit;
    }
    prev_heap = heap;
    heap += incr;
    SYSTEM_DEBUGF(SYSCALL_DEBUG_INFO, "Memory has been allocated.\tHeap-address: %p", prev_heap);
    return (caddr_t) prev_heap;
}

/**
  * @brief  make a new path for a file
  * @param  *reent
  * @param  old path
  * @param  new path
  * @retval 0..OK -1..error
  */
__attribute__ ((used))
int link_r(struct _reent *reent, char *old, char *new)
{
    SYSTEM_DEBUGF(SYSCALL_DEBUG_INFO, "Change path old: %s, new: %s",old, new);
    reent->_errno = EMLINK;
    return -1;
}

/**
  * @brief  delete a name and possibly the file it refers to
  * @param  *reent
  * @param  filepath
  * @retval 0..OK -1..error
  */
int _unlink_r(struct _reent *reent, const char *name )
{
    SYSTEM_DEBUGF(SYSCALL_DEBUG_INFO, "Delete path: %s", name);
    reent->_errno = EMLINK;
    return -1;
}

/**
  * @brief  creates a new open file description
  * @param  *reent
  * @param  path to new file
  * @param  flags for access mode
  * @param  permissions for the new file
  * @retval new file descriptor
  */
__attribute__ ((used))
int _open_r(struct _reent *reent, const char *file, int flags, int mode)
{
    static bool StdVolMount = false; // FatFs initialization state
    uint32_t which_devoptab = 0;
    uint32_t fd = -1; // file descriptor

    /********** hardware resource ***********/
    /* call hardware resource through files */
    /* search for "file" in dotab_list[].name */
    while( devoptab_list[which_devoptab] != 0 )
    {
        if( strcmp(devoptab_list[which_devoptab]->name, file ) == 0 )
        {
            fd = which_devoptab;
            break;
        }
        which_devoptab++;
    }

    /* if we found the requested file/device, invoke the device's open_r() */
    if( fd != -1 )
    {
        SYSTEM_DEBUGF(SYSCALL_DEBUG, "Hardware resource %d opened.", fd);
        return devoptab_list[fd]->open(file, flags, mode );
    }

    /**************** FatFs *****************/
    /* if no hardware resource was found - create a file on a mass storage device (e.g. SD-Card) */
    fd = FATFS_STARTINDEX;
    while(IsOpenFile[fd] != false) // Search next free file descriptor
        fd++;

    if(fd < MAX_OPEN_FILES)
    {
        if(!StdVolMount)
        {
            TM_DELAY_Init(); //Initialize FatFs delays
            //Mount drive
            if (f_mount(&FatFs, "", 1) == FR_OK)
            {
                // FatFs standard volume is mounted
                StdVolMount = true;
            }
            else
            {
                SYSTEM_DEBUGF(SYSCALL_DEBUG, "Standard volume failed to mount.",0);
                reent->_errno = EIO;
                return -1; // error - No file created!
            }
        }


        if (f_open(&Files[fd], file, FA_OPEN_ALWAYS | FA_READ | FA_WRITE) == FR_OK)   //Try to open file
        {
            IsOpenFile[fd] = true; // File is used
            SYSTEM_DEBUGF(SYSCALL_DEBUG, "File ID: %ld\topened successfully.", fd);
            return fd;
        }
        else
        {
            SYSTEM_DEBUGF(SYSCALL_DEBUG, "File ID: %ld\tfailed to open.", fd);
            reent->_errno = EIO;
            return -1;
        }
        //f_mount(0, "", 1);
    }
    else
    {
        SYSTEM_DEBUGF(SYSCALL_DEBUG, "Too many files are open.",0);
        reent->_errno = ENODEV;
        return -1; // error - No file created!
    }
}

/**
  * @brief  attempts to read up to count bytes from file descriptor fd into
  *         buffer starting at buf.
  * @param  *reent
  * @param  file descriptor
  * @param  pointer at buffer
  * @param  number of bytes to read
  * @retval On success the number of bytes is returned, on error -1 is returned
  */
__attribute__ ((used))
int _read_r(struct _reent *reent, int file, char *ptr, int len)
{
    /*************** semihosting ************/
#if SYSTEM_STDIO_SEMIHOSTING
    // Use Semihosting only in Debug Mode, otherwise program would not run in normal mode.
    if(SH_IsDebugMode())
        return SH_SendData(ptr, len);
#endif
    SYSTEM_DEBUGF(SYSCALL_DEBUG_INFO, "File-ID: %d\tread function called.",file);

    /*************** check param ************/
    if(file >= MAX_OPEN_FILES)
    {
        reent->_errno = EBADF; // bad file
        SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS,
                      "File ID: %d\t out of range.",file);
        return -1;
    }

    if(file < FATFS_STARTINDEX)
    {
        /********** hardware resource ***********/
        return devoptab_list[file]->read(file, ptr, len);
    }
    else
    {
        /**************** FatFs *****************/
        UINT BytesWritten = 0;
        FRESULT err;

        // check if file is open
        if(IsOpenFile[file] == false){
            reent->_errno = EBADF;
            SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS, "File ID: %d\tFile not open.",file);
            return -1;
        }

        err = f_read(&Files[file], ptr, len, &BytesWritten);
        if(err == FR_OK)
        {
            SYSTEM_DEBUGF(SYSCALL_DEBUG_INFO, "File ID: %d\tRead from file successful.",file);
            return BytesWritten;
        }

        else if(err == FR_DISK_ERR)
        {
            reent->_errno = EIO;
            SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS, "File ID: %d\tDisk error.",file);
            return BytesWritten;
        }
        else if(err == FR_INT_ERR)
        {
            reent->_errno = EIO;
            SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS, "File ID: %d\tInternal error.",file);
            return BytesWritten;
        }
        else if(err == FR_NOT_READY)
        {
            reent->_errno = EBUSY;
            SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS, "File ID: %d\tDisk not ready.",file);
            return BytesWritten;
        }
        else if(err == FR_INVALID_OBJECT)
        {
            reent->_errno = EIO;
            SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS, "File ID: %d\tInvalid object.",file);
            return BytesWritten;
        }
        else if(err == FR_TIMEOUT)
        {
            reent->_errno = EIO;
            SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS, "Timeout.",0);
            return BytesWritten;
        }
    }
}

/**
  * @brief  writes to the file referenced by the file descriptor
  * @param  *reent
  * @param  file descriptor
  * @param  pointer at buffer
  * @param  number of bytes to write
  * @retval On success the number of bytes is returned, on error -1 is returned
  */
__attribute__ ((used))
int _write_r(struct _reent *reent, int file, char *ptr, int len)
{
    /*************** semihosting ************/
#if SYSTEM_STDIO_SEMIHOSTING
    // Use Semihosting only in Debug Mode, otherwise program would not run in normal mode.
    if(SH_IsDebugMode())
        return SH_SendData(ptr, len);
#endif


    /*************** check param ************/
    if(file >= MAX_OPEN_FILES)
    {
        reent->_errno = EBADF; // bad file
        SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS,
                      "File ID: %d\t out of range.",file);
        return -1;
    }

    if(file < FATFS_STARTINDEX)
    {
        /********** hardware resource ***********/
        return devoptab_list[file]->write(file, ptr, len);
    }
    else
    {
        /**************** FatFs *****************/
        UINT BytesWritten = 0;
        FRESULT err;

        // check if file is open
        if(IsOpenFile[file] == false){
            reent->_errno = EBADF;
            SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS, "File ID: %d\tFile not open.",file);
            return -1;
        }

        err = f_write(&Files[file], ptr, len, &BytesWritten);
        if(err == FR_OK)
        {
            SYSTEM_DEBUGF(SYSCALL_DEBUG_INFO, "File ID: %d\tWrite to file successful.",file);
            return BytesWritten;
        }

        else if(err == FR_DISK_ERR)
        {
            reent->_errno = EIO;
            SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS, "File ID: %d\tDisk error.",file);
            return BytesWritten;
        }
        else if(err == FR_INT_ERR)
        {
            reent->_errno = EIO;
            SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS, "File ID: %d\tInternal error.",file);
            return BytesWritten;
        }
        else if(err == FR_NOT_READY)
        {
            reent->_errno = EBUSY;
            SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS, "File ID: %d\tDisk not ready.",file);
            return BytesWritten;
        }
        else if(err == FR_INVALID_OBJECT)
        {
            reent->_errno = EIO;
            SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS, "File ID: %d\tInvalid object.",file);
            return BytesWritten;
        }
        else if(err == FR_TIMEOUT)
        {
            reent->_errno = EIO;
            SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS, "Timeout.",0);
            return BytesWritten;
        }
    }
}

/**
  * @brief  closes a file descriptor, so that it no longer refers to any file
  *         and may be reused.
  * @param  *reent
  * @param  file descriptor
  * @retval On success 0 is returned, on error -1 is returned
  */
__attribute__ ((used))
int _close1(struct _reent *reent, int file)
{
    /**************** FatFs *****************/
    // f_close(&Files[fd]);

    /*************** check param ************/
    if(file >= MAX_OPEN_FILES)
    {
        reent->_errno = EBADF; // bad file
        SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS,
            "File ID: %d\tout of range.",file);
        return -1;
    }

    if(file < FATFS_STARTINDEX)
    {
        /********** hardware resource ***********/
        SYSTEM_DEBUGF(SYSCALL_DEBUG_INFO, "Hardware resource \"%s\" closed.",
            devoptab_list[file]->name);
        return devoptab_list[file]->close(file);
    }
    else
    {
        /**************** FatFs *****************/
        FRESULT err;

        // check if file is open
        if(IsOpenFile[file] == false){
            reent->_errno = EBADF;
            SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS, "File ID: %d\tFile not open.",file);
            return -1;
        }

        err = f_close(&Files[file]);
        if(err == FR_OK)
        {
            IsOpenFile[file] = false; // free file
            SYSTEM_DEBUGF(SYSCALL_DEBUG_INFO, "File ID: %d\tclosed.",file);
            return 0;
        }
        else
        {
            SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS,
                "File ID: %d\tfailed to close.\n",file);
            return -1;
        }
    }
}


/**
  * @brief  These functions return information about a file.
  *         No permissions are required on the file itself, but � in the case
  *         of stat() and lstat() � execute (search) permission is required on
  *         all of the directories in path that lead to the file.
  * @param  *reent
  * @param  system calls return a stat structure, which contains file info
  * @retval On success 0 is returned, on error -1 is returned
  */
int _stat_r(struct _reent *reent, const char *file, struct stat *pstat)
{
    SYSTEM_DEBUGF(SYSCALL_DEBUG_INFO, "Fd: %d\t_stat called.",file);
    pstat->st_mode = S_IFCHR;

////C runtime
//    struct stat {
//    dev_t     st_dev;     /* ID of device containing file */
//    ino_t     st_ino;     /* inode number */
//    mode_t    st_mode;    /* protection */
//    nlink_t   st_nlink;   /* number of hard links */
//    uid_t     st_uid;     /* user ID of owner */
//    gid_t     st_gid;     /* group ID of owner */
//    dev_t     st_rdev;    /* device ID (if special file) */
//    off_t     st_size;    /* total size, in bytes */
//    blksize_t st_blksize; /* blocksize for filesystem I/O */
//    blkcnt_t  st_blocks;  /* number of blocks allocated */
//    time_t    st_atime;   /* time of last access */
//    time_t    st_mtime;   /* time of last modification */
//    time_t    st_ctime;   /* time of last status change */
//
//// fatfs
//    typedef struct {
//    DWORD fsize;      /* File size */
//    WORD  fdate;      /* Last modified date */
//    WORD  ftime;      /* Last modified time */
//    BYTE  fattrib;    /* Attribute */
//    TCHAR fname[13];  /* Short file name (8.3 format) */
//#if _USE_LFN
//    TCHAR* lfname;    /* Pointer to the LFN buffer */
//    int   lfsize;     /* Size of the LFN buffer in unit of TCHAR */
//#endif
//} FILINFO;

    return 0;
}

__attribute__ ((used))
int _fstat_r(struct _reent *reent, int file, struct stat *st)
{
    SYSTEM_DEBUGF(SYSCALL_DEBUG_INFO, "Fd: %d\t_fstat called.",file);
    st->st_mode = S_IFCHR;
    return 0;
}


/**
  * @brief  returns 1 if fd is an open file descriptor referring to a terminal;
  *         otherwise 0 is returned, and errno is set to indicate the error.
  * @param  *reent
  * @param  file descriptor
  * @retval 1..open file to terminal 0..otherwise
  */
__attribute__ ((used))
int _isatty_r(struct _reent *reent, int file)
{
//  SYSTEM_DEBUGF(SYSCALL_DEBUG_INFO, "Fd: %d\t_isatty called.",file);
//    return 1;

    /**************** FatFs *****************/
    // f_close(&Files[fd]);

    /*************** check param ************/
    if(file >= MAX_OPEN_FILES)
    {
        reent->_errno = EBADF; // bad file
        SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS,
                      "File ID: %d\tout of range.",file);
        return -1;
    }

    if(file < FATFS_STARTINDEX)
    {
        /********** hardware resource ***********/
        SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS,
                      "File ID: %d\treferring to hardware device.",file);
        return 1;
    }
    else
    {
        /**************** FatFs *****************/
        SYSTEM_DEBUGF(SYSCALL_DEBUG_SERIOUS,
                      "File ID: %d\treferring to file system.",file);
        return 0;
    }
}

__attribute__ ((used))
int _lseek_r(struct _reent *reent, int file, int ptr, int dir)
{
  SYSTEM_DEBUGF(SYSCALL_DEBUG_INFO, "Fd: %d\t_lseek called.",file);
    return 0;
}




__attribute__ ((used))
void abort(void)
{
    /* Abort called */
    while(1);
}

int _times_r (struct _reent *reent, struct tms *tmsbuf )
{
  SYSTEM_DEBUGF(SYSCALL_DEBUG_INFO, "_times called.",0);
    return -1;
}

/**
  * @}
  */
/* --------------------------------- End Of File ------------------------------ */
