/**
  * @file    ./SystemCore/src/main.c 
  * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
  * @brief   main module
  */
  
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern uint8_t taskclass4_signal;

/* Private function prototypes -----------------------------------------------*/
extern void startup_sequence(void);
extern void taskclass1(void);
extern void taskclass2(void);
extern void taskclass3(void);
extern void taskclass4(void);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
  startup_sequence();

  while(1)
  {
    if(taskclass4_signal)
    {
      taskclass4();
      taskclass4_signal = 0;
    }
  }
}
