/**
  ******************************************************************************
  * @file    Project/STM32F4xx_StdPeriph_Templates/stm32f4xx_it.c 
  * @author  MCD Application Team
  * @version V1.8.0
  * @date    04-November-2016
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2016 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_it.h"

/** @addtogroup Template_Project
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t taskclass4_signal;

/* Private function prototypes -----------------------------------------------*/
extern void taskclass1(void);
extern void taskclass2(void);
extern void taskclass3(void);
extern void taskclass4(void);
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
#ifdef __USING_DEBUG
  LIB_DEBUG_hardfault_handler_wrapper();
#endif // __USING_DEBUG

  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{

}

/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f4xx.s).                                               */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

#ifdef __USING_TIMER

/**
  * @brief  This function handles TIM1_TRG_COM_TIM11_IR
  * @param  None
  * @retval None
  */
void TIM1_TRG_COM_TIM11_IRQHandler(void){
  taskclass1();

  TIM_ClearITPendingBit(TIM11, TIM_IT_Update);
}

/**
  * @brief  This function handles TIM8_UP_TIM13_IR
  * @param  None
  * @retval None
  */
void TIM8_UP_TIM13_IRQHandler(void){
  taskclass2();

  TIM_ClearITPendingBit(TIM13, TIM_IT_Update);
}

/**
  * @brief  This function handles TIM8_TRG_COM_TIM14_IR
  * @param  None
  * @retval None
  */
void TIM8_TRG_COM_TIM14_IRQHandler(void){

    /* timing for taskclass4 */
    static double  t = 0;

    if(t + T_PERIOD3_S < T_PERIOD4_S)
    {
        t = t + T_PERIOD3_S;
    }
    else
    {
        t = t + T_PERIOD3_S - T_PERIOD4_S;
        taskclass4_signal = 1;
    }

    taskclass3();

    TIM_ClearITPendingBit(TIM14, TIM_IT_Update);
}

#endif // __USING_TIMER

#ifdef __USING_USART

//USART1 Rx
void DMA2_Stream2_IRQHandler(void)
{
  if (DMA_GetITStatus(DMA2_Stream2, DMA_IT_TCIF2))
  {
    // Flag zuruecksetzen
    DMA_ClearITPendingBit(DMA2_Stream2, DMA_IT_TCIF2);
  }
}

//USART1 Tx
void DMA2_Stream7_IRQHandler(void)
{
  // Test auf Transfer-Complete Interrupt Flag
  if (DMA_GetITStatus(DMA2_Stream7, DMA_IT_TCIF7))
  {
    API_USART1.busy = 0;

    // Flags zuruecksetzen
    DMA_ClearITPendingBit(DMA2_Stream7, DMA_IT_TCIF7);
  }
}

//USART2 Rx
void DMA1_Stream5_IRQHandler(void)
{
  if (DMA_GetITStatus(DMA1_Stream5, DMA_IT_TCIF5))
  {

    // Flag zuruecksetzen
    DMA_ClearITPendingBit(DMA1_Stream5, DMA_IT_TCIF5);
  }
}

//USART2 Tx
void DMA1_Stream6_IRQHandler(void)
{
  // Test auf Transfer-Complete Interrupt Flag
  if (DMA_GetITStatus(DMA1_Stream6, DMA_IT_TCIF6))
  {
    API_USART2.busy = 0;

    // Flags zuruecksetzen
    DMA_ClearITPendingBit(DMA1_Stream6, DMA_IT_TCIF6);
  }
}

//USART3 Rx
void DMA1_Stream1_IRQHandler(void)
{
  if (DMA_GetITStatus(DMA1_Stream1, DMA_IT_TCIF1))
  {

    // Flag zuruecksetzen
    DMA_ClearITPendingBit(DMA1_Stream1, DMA_IT_TCIF1);
  }
}

//USART3 Tx
void DMA1_Stream3_IRQHandler(void)
{
  // Test auf Transfer-Complete Interrupt Flag
  if (DMA_GetITStatus(DMA1_Stream3, DMA_IT_TCIF3))
  {

    // Flags zuruecksetzen
    DMA_ClearITPendingBit(DMA1_Stream3, DMA_IT_TCIF3);
  }
}

//UART4 Rx
void DMA1_Stream2_IRQHandler(void)
{
  if (DMA_GetITStatus(DMA1_Stream2, DMA_IT_TCIF2))
  {

    // Flag zuruecksetzen
    DMA_ClearITPendingBit(DMA1_Stream2, DMA_IT_TCIF2);
  }
}

//UART4 Tx
void DMA1_Stream4_IRQHandler(void)
{
  // Test auf Transfer-Complete Interrupt Flag
  if (DMA_GetITStatus(DMA1_Stream4, DMA_IT_TCIF4))
  {
    API_USART4.busy = 0;

    // Flags zuruecksetzen
    DMA_ClearITPendingBit(DMA1_Stream4, DMA_IT_TCIF4);
  }
}

//UART5 Rx
void DMA1_Stream0_IRQHandler(void)
{
  if (DMA_GetITStatus(DMA1_Stream0, DMA_IT_TCIF0))
  {

    // Flag zuruecksetzen
    DMA_ClearITPendingBit(DMA1_Stream0, DMA_IT_TCIF0);
  }
}

//UART5 Tx
void DMA1_Stream7_IRQHandler(void)
{
  // Test auf Transfer-Complete Interrupt Flag
  if (DMA_GetITStatus(DMA1_Stream7, DMA_IT_TCIF7))
  {
    API_USART5.busy = 0;

    // Flags zuruecksetzen
    DMA_ClearITPendingBit(DMA1_Stream7, DMA_IT_TCIF7);
  }
}

//USART6 Rx
void DMA2_Stream1_IRQHandler(void)
{
  if (DMA_GetITStatus(DMA2_Stream1, DMA_IT_TCIF1))
  {

    // Flag zuruecksetzen
    DMA_ClearITPendingBit(DMA2_Stream1, DMA_IT_TCIF1);
  }
}

//USART6 Tx
void DMA2_Stream6_IRQHandler(void)
{
  // Test auf Transfer-Complete Interrupt Flag
  if (DMA_GetITStatus(DMA2_Stream6, DMA_IT_TCIF6))
  {
    API_USART6.busy = 0;

    // Flags zuruecksetzen
    DMA_ClearITPendingBit(DMA2_Stream6, DMA_IT_TCIF6);
  }
}

#endif // __USING_USART

/**
  * @}
  */ 


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
