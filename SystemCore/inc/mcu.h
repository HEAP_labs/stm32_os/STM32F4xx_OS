/**
  ******************************************************************************
  * @file    ./System_Core/inc/mcu.h
  * @author  Andreas Hirtenlehner
  * @brief   MCU definition
  */

#ifndef __MCU_H
#define __MCU_H

/* Exported constants --------------------------------------------------------*/
#ifndef STM32F40_41xxx
    #define STM32F40_41xxx
#endif // !STM32F40_41xxx

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"

/* Exported types ------------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif
