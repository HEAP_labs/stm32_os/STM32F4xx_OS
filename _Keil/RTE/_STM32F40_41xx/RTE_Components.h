
/*
 * Auto generated Run-Time-Environment Component Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'STM32F4xx_OS' 
 * Target:  'STM32F40_41xx' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H


/*
 * Define the Device Header File: 
 */
#define CMSIS_device_header "stm32f4xx.h"

#define RTE_DEVICE_STARTUP_STM32F4XX    /* Device Startup for STM32F4 */

#endif /* RTE_COMPONENTS_H */
