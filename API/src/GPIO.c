/**
 * @file    ./API/src/GPIO.c
 * @author  Andreas Hirtenlehner, Gerald Ebmer
 * @brief   API for GPIO
 */
  
#include "PerUsings.h"
#ifdef __USING_GPIO

/* Includes ------------------------------------------------------------------*/
#include "GPIO.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
API_GPIO_type_t PA0 = { GPIOA, GPIO_Pin_0, RCC_AHB1Periph_GPIOA };
API_GPIO_type_t PA1 = { GPIOA, GPIO_Pin_1, RCC_AHB1Periph_GPIOA };
API_GPIO_type_t PA2 = { GPIOA, GPIO_Pin_2, RCC_AHB1Periph_GPIOA };
API_GPIO_type_t PA3 = { GPIOA, GPIO_Pin_3, RCC_AHB1Periph_GPIOA };
API_GPIO_type_t PA4 = { GPIOA, GPIO_Pin_4, RCC_AHB1Periph_GPIOA };
API_GPIO_type_t PA5 = { GPIOA, GPIO_Pin_5, RCC_AHB1Periph_GPIOA };
API_GPIO_type_t PA6 = { GPIOA, GPIO_Pin_6, RCC_AHB1Periph_GPIOA };
API_GPIO_type_t PA7 = { GPIOA, GPIO_Pin_7, RCC_AHB1Periph_GPIOA };
API_GPIO_type_t PA8 = { GPIOA, GPIO_Pin_8, RCC_AHB1Periph_GPIOA };
API_GPIO_type_t PA9 = { GPIOA, GPIO_Pin_9, RCC_AHB1Periph_GPIOA };
API_GPIO_type_t PA10 = { GPIOA, GPIO_Pin_10, RCC_AHB1Periph_GPIOA };
API_GPIO_type_t PA11 = { GPIOA, GPIO_Pin_11, RCC_AHB1Periph_GPIOA };
API_GPIO_type_t PA12 = { GPIOA, GPIO_Pin_12, RCC_AHB1Periph_GPIOA };
API_GPIO_type_t PA13 = { GPIOA, GPIO_Pin_13, RCC_AHB1Periph_GPIOA };
API_GPIO_type_t PA14 = { GPIOA, GPIO_Pin_14, RCC_AHB1Periph_GPIOA };
API_GPIO_type_t PA15 = { GPIOA, GPIO_Pin_15, RCC_AHB1Periph_GPIOA };

API_GPIO_type_t PB0 = { GPIOB, GPIO_Pin_0, RCC_AHB1Periph_GPIOB };
API_GPIO_type_t PB1 = { GPIOB, GPIO_Pin_1, RCC_AHB1Periph_GPIOB };
API_GPIO_type_t PB2 = { GPIOB, GPIO_Pin_2, RCC_AHB1Periph_GPIOB };
API_GPIO_type_t PB3 = { GPIOB, GPIO_Pin_3, RCC_AHB1Periph_GPIOB };
API_GPIO_type_t PB4 = { GPIOB, GPIO_Pin_4, RCC_AHB1Periph_GPIOB };
API_GPIO_type_t PB5 = { GPIOB, GPIO_Pin_5, RCC_AHB1Periph_GPIOB };
API_GPIO_type_t PB6 = { GPIOB, GPIO_Pin_6, RCC_AHB1Periph_GPIOB };
API_GPIO_type_t PB7 = { GPIOB, GPIO_Pin_7, RCC_AHB1Periph_GPIOB };
API_GPIO_type_t PB8 = { GPIOB, GPIO_Pin_8, RCC_AHB1Periph_GPIOB };
API_GPIO_type_t PB9 = { GPIOB, GPIO_Pin_9, RCC_AHB1Periph_GPIOB };
API_GPIO_type_t PB10 = { GPIOB, GPIO_Pin_10, RCC_AHB1Periph_GPIOB };
API_GPIO_type_t PB11 = { GPIOB, GPIO_Pin_11, RCC_AHB1Periph_GPIOB };
API_GPIO_type_t PB12 = { GPIOB, GPIO_Pin_12, RCC_AHB1Periph_GPIOB };
API_GPIO_type_t PB13 = { GPIOB, GPIO_Pin_13, RCC_AHB1Periph_GPIOB };
API_GPIO_type_t PB14 = { GPIOB, GPIO_Pin_14, RCC_AHB1Periph_GPIOB };
API_GPIO_type_t PB15 = { GPIOB, GPIO_Pin_15, RCC_AHB1Periph_GPIOB };

API_GPIO_type_t PC0 = { GPIOC, GPIO_Pin_0, RCC_AHB1Periph_GPIOC };
API_GPIO_type_t PC1 = { GPIOC, GPIO_Pin_1, RCC_AHB1Periph_GPIOC };
API_GPIO_type_t PC2 = { GPIOC, GPIO_Pin_2, RCC_AHB1Periph_GPIOC };
API_GPIO_type_t PC3 = { GPIOC, GPIO_Pin_3, RCC_AHB1Periph_GPIOC };
API_GPIO_type_t PC4 = { GPIOC, GPIO_Pin_4, RCC_AHB1Periph_GPIOC };
API_GPIO_type_t PC5 = { GPIOC, GPIO_Pin_5, RCC_AHB1Periph_GPIOC };
API_GPIO_type_t PC6 = { GPIOC, GPIO_Pin_6, RCC_AHB1Periph_GPIOC };
API_GPIO_type_t PC7 = { GPIOC, GPIO_Pin_7, RCC_AHB1Periph_GPIOC };
API_GPIO_type_t PC8 = { GPIOC, GPIO_Pin_8, RCC_AHB1Periph_GPIOC };
API_GPIO_type_t PC9 = { GPIOC, GPIO_Pin_9, RCC_AHB1Periph_GPIOC };
API_GPIO_type_t PC10 = { GPIOC, GPIO_Pin_10, RCC_AHB1Periph_GPIOC };
API_GPIO_type_t PC11 = { GPIOC, GPIO_Pin_11, RCC_AHB1Periph_GPIOC };
API_GPIO_type_t PC12 = { GPIOC, GPIO_Pin_12, RCC_AHB1Periph_GPIOC };
API_GPIO_type_t PC13 = { GPIOC, GPIO_Pin_13, RCC_AHB1Periph_GPIOC };
API_GPIO_type_t PC14 = { GPIOC, GPIO_Pin_14, RCC_AHB1Periph_GPIOC };
API_GPIO_type_t PC15 = { GPIOC, GPIO_Pin_15, RCC_AHB1Periph_GPIOC };

API_GPIO_type_t PD0 = { GPIOD, GPIO_Pin_0, RCC_AHB1Periph_GPIOD };
API_GPIO_type_t PD1 = { GPIOD, GPIO_Pin_1, RCC_AHB1Periph_GPIOD };
API_GPIO_type_t PD2 = { GPIOD, GPIO_Pin_2, RCC_AHB1Periph_GPIOD };
API_GPIO_type_t PD3 = { GPIOD, GPIO_Pin_3, RCC_AHB1Periph_GPIOD };
API_GPIO_type_t PD4 = { GPIOD, GPIO_Pin_4, RCC_AHB1Periph_GPIOD };
API_GPIO_type_t PD5 = { GPIOD, GPIO_Pin_5, RCC_AHB1Periph_GPIOD };
API_GPIO_type_t PD6 = { GPIOD, GPIO_Pin_6, RCC_AHB1Periph_GPIOD };
API_GPIO_type_t PD7 = { GPIOD, GPIO_Pin_7, RCC_AHB1Periph_GPIOD };
API_GPIO_type_t PD8 = { GPIOD, GPIO_Pin_8, RCC_AHB1Periph_GPIOD };
API_GPIO_type_t PD9 = { GPIOD, GPIO_Pin_9, RCC_AHB1Periph_GPIOD };
API_GPIO_type_t PD10 = { GPIOD, GPIO_Pin_10, RCC_AHB1Periph_GPIOD };
API_GPIO_type_t PD11 = { GPIOD, GPIO_Pin_11, RCC_AHB1Periph_GPIOD };
API_GPIO_type_t PD12 = { GPIOD, GPIO_Pin_12, RCC_AHB1Periph_GPIOD };
API_GPIO_type_t PD13 = { GPIOD, GPIO_Pin_13, RCC_AHB1Periph_GPIOD };
API_GPIO_type_t PD14 = { GPIOD, GPIO_Pin_14, RCC_AHB1Periph_GPIOD };
API_GPIO_type_t PD15 = { GPIOD, GPIO_Pin_15, RCC_AHB1Periph_GPIOD };

API_GPIO_type_t PE0 = { GPIOE, GPIO_Pin_0, RCC_AHB1Periph_GPIOE };
API_GPIO_type_t PE1 = { GPIOE, GPIO_Pin_1, RCC_AHB1Periph_GPIOE };
API_GPIO_type_t PE2 = { GPIOE, GPIO_Pin_2, RCC_AHB1Periph_GPIOE };
API_GPIO_type_t PE3 = { GPIOE, GPIO_Pin_3, RCC_AHB1Periph_GPIOE };
API_GPIO_type_t PE4 = { GPIOE, GPIO_Pin_4, RCC_AHB1Periph_GPIOE };
API_GPIO_type_t PE5 = { GPIOE, GPIO_Pin_5, RCC_AHB1Periph_GPIOE };
API_GPIO_type_t PE6 = { GPIOE, GPIO_Pin_6, RCC_AHB1Periph_GPIOE };
API_GPIO_type_t PE7 = { GPIOE, GPIO_Pin_7, RCC_AHB1Periph_GPIOE };
API_GPIO_type_t PE8 = { GPIOE, GPIO_Pin_8, RCC_AHB1Periph_GPIOE };
API_GPIO_type_t PE9 = { GPIOE, GPIO_Pin_9, RCC_AHB1Periph_GPIOE };
API_GPIO_type_t PE10 = { GPIOE, GPIO_Pin_10, RCC_AHB1Periph_GPIOE };
API_GPIO_type_t PE11 = { GPIOE, GPIO_Pin_11, RCC_AHB1Periph_GPIOE };
API_GPIO_type_t PE12 = { GPIOE, GPIO_Pin_12, RCC_AHB1Periph_GPIOE };
API_GPIO_type_t PE13 = { GPIOE, GPIO_Pin_13, RCC_AHB1Periph_GPIOE };
API_GPIO_type_t PE14 = { GPIOE, GPIO_Pin_14, RCC_AHB1Periph_GPIOE };
API_GPIO_type_t PE15 = { GPIOE, GPIO_Pin_15, RCC_AHB1Periph_GPIOE };

API_GPIO_type_t PF0 = { GPIOF, GPIO_Pin_0, RCC_AHB1Periph_GPIOF };
API_GPIO_type_t PF1 = { GPIOF, GPIO_Pin_1, RCC_AHB1Periph_GPIOF };
API_GPIO_type_t PF2 = { GPIOF, GPIO_Pin_2, RCC_AHB1Periph_GPIOF };
API_GPIO_type_t PF3 = { GPIOF, GPIO_Pin_3, RCC_AHB1Periph_GPIOF };
API_GPIO_type_t PF4 = { GPIOF, GPIO_Pin_4, RCC_AHB1Periph_GPIOF };
API_GPIO_type_t PF5 = { GPIOF, GPIO_Pin_5, RCC_AHB1Periph_GPIOF };
API_GPIO_type_t PF6 = { GPIOF, GPIO_Pin_6, RCC_AHB1Periph_GPIOF };
API_GPIO_type_t PF7 = { GPIOF, GPIO_Pin_7, RCC_AHB1Periph_GPIOF };
API_GPIO_type_t PF8 = { GPIOF, GPIO_Pin_8, RCC_AHB1Periph_GPIOF };
API_GPIO_type_t PF9 = { GPIOF, GPIO_Pin_9, RCC_AHB1Periph_GPIOF };
API_GPIO_type_t PF10 = { GPIOF, GPIO_Pin_10, RCC_AHB1Periph_GPIOF };
API_GPIO_type_t PF11 = { GPIOF, GPIO_Pin_11, RCC_AHB1Periph_GPIOF };
API_GPIO_type_t PF12 = { GPIOF, GPIO_Pin_12, RCC_AHB1Periph_GPIOF };
API_GPIO_type_t PF13 = { GPIOF, GPIO_Pin_13, RCC_AHB1Periph_GPIOF };
API_GPIO_type_t PF14 = { GPIOF, GPIO_Pin_14, RCC_AHB1Periph_GPIOF };
API_GPIO_type_t PF15 = { GPIOF, GPIO_Pin_15, RCC_AHB1Periph_GPIOF };

API_GPIO_type_t PG0 = { GPIOG, GPIO_Pin_0, RCC_AHB1Periph_GPIOG };
API_GPIO_type_t PG1 = { GPIOG, GPIO_Pin_1, RCC_AHB1Periph_GPIOG };
API_GPIO_type_t PG2 = { GPIOG, GPIO_Pin_2, RCC_AHB1Periph_GPIOG };
API_GPIO_type_t PG3 = { GPIOG, GPIO_Pin_3, RCC_AHB1Periph_GPIOG };
API_GPIO_type_t PG4 = { GPIOG, GPIO_Pin_4, RCC_AHB1Periph_GPIOG };
API_GPIO_type_t PG5 = { GPIOG, GPIO_Pin_5, RCC_AHB1Periph_GPIOG };
API_GPIO_type_t PG6 = { GPIOG, GPIO_Pin_6, RCC_AHB1Periph_GPIOG };
API_GPIO_type_t PG7 = { GPIOG, GPIO_Pin_7, RCC_AHB1Periph_GPIOG };
API_GPIO_type_t PG8 = { GPIOG, GPIO_Pin_8, RCC_AHB1Periph_GPIOG };
API_GPIO_type_t PG9 = { GPIOG, GPIO_Pin_9, RCC_AHB1Periph_GPIOG };
API_GPIO_type_t PG10 = { GPIOG, GPIO_Pin_10, RCC_AHB1Periph_GPIOG };
API_GPIO_type_t PG11 = { GPIOG, GPIO_Pin_11, RCC_AHB1Periph_GPIOG };
API_GPIO_type_t PG12 = { GPIOG, GPIO_Pin_12, RCC_AHB1Periph_GPIOG };
API_GPIO_type_t PG13 = { GPIOG, GPIO_Pin_13, RCC_AHB1Periph_GPIOG };
API_GPIO_type_t PG14 = { GPIOG, GPIO_Pin_14, RCC_AHB1Periph_GPIOG };
API_GPIO_type_t PG15 = { GPIOG, GPIO_Pin_15, RCC_AHB1Periph_GPIOG };

API_GPIO_type_t PH0 = { GPIOH, GPIO_Pin_0, RCC_AHB1Periph_GPIOH };
API_GPIO_type_t PH1 = { GPIOH, GPIO_Pin_1, RCC_AHB1Periph_GPIOH };
API_GPIO_type_t PH2 = { GPIOH, GPIO_Pin_2, RCC_AHB1Periph_GPIOH };
API_GPIO_type_t PH3 = { GPIOH, GPIO_Pin_3, RCC_AHB1Periph_GPIOH };
API_GPIO_type_t PH4 = { GPIOH, GPIO_Pin_4, RCC_AHB1Periph_GPIOH };
API_GPIO_type_t PH5 = { GPIOH, GPIO_Pin_5, RCC_AHB1Periph_GPIOH };
API_GPIO_type_t PH6 = { GPIOH, GPIO_Pin_6, RCC_AHB1Periph_GPIOH };
API_GPIO_type_t PH7 = { GPIOH, GPIO_Pin_7, RCC_AHB1Periph_GPIOH };
API_GPIO_type_t PH8 = { GPIOH, GPIO_Pin_8, RCC_AHB1Periph_GPIOH };
API_GPIO_type_t PH9 = { GPIOH, GPIO_Pin_9, RCC_AHB1Periph_GPIOH };
API_GPIO_type_t PH10 = { GPIOH, GPIO_Pin_10, RCC_AHB1Periph_GPIOH };
API_GPIO_type_t PH11 = { GPIOH, GPIO_Pin_11, RCC_AHB1Periph_GPIOH };
API_GPIO_type_t PH12 = { GPIOH, GPIO_Pin_12, RCC_AHB1Periph_GPIOH };
API_GPIO_type_t PH13 = { GPIOH, GPIO_Pin_13, RCC_AHB1Periph_GPIOH };
API_GPIO_type_t PH14 = { GPIOH, GPIO_Pin_14, RCC_AHB1Periph_GPIOH };
API_GPIO_type_t PH15 = { GPIOH, GPIO_Pin_15, RCC_AHB1Periph_GPIOH };

API_GPIO_type_t PI0 = { GPIOI, GPIO_Pin_0, RCC_AHB1Periph_GPIOI };
API_GPIO_type_t PI1 = { GPIOI, GPIO_Pin_1, RCC_AHB1Periph_GPIOI };
API_GPIO_type_t PI2 = { GPIOI, GPIO_Pin_2, RCC_AHB1Periph_GPIOI };
API_GPIO_type_t PI3 = { GPIOI, GPIO_Pin_3, RCC_AHB1Periph_GPIOI };
API_GPIO_type_t PI4 = { GPIOI, GPIO_Pin_4, RCC_AHB1Periph_GPIOI };
API_GPIO_type_t PI5 = { GPIOI, GPIO_Pin_5, RCC_AHB1Periph_GPIOI };
API_GPIO_type_t PI6 = { GPIOI, GPIO_Pin_6, RCC_AHB1Periph_GPIOI };
API_GPIO_type_t PI7 = { GPIOI, GPIO_Pin_7, RCC_AHB1Periph_GPIOI };
API_GPIO_type_t PI8 = { GPIOI, GPIO_Pin_8, RCC_AHB1Periph_GPIOI };
API_GPIO_type_t PI9 = { GPIOI, GPIO_Pin_9, RCC_AHB1Periph_GPIOI };
API_GPIO_type_t PI10 = { GPIOI, GPIO_Pin_10, RCC_AHB1Periph_GPIOI };
API_GPIO_type_t PI11 = { GPIOI, GPIO_Pin_11, RCC_AHB1Periph_GPIOI };
API_GPIO_type_t PI12 = { GPIOI, GPIO_Pin_12, RCC_AHB1Periph_GPIOI };
API_GPIO_type_t PI13 = { GPIOI, GPIO_Pin_13, RCC_AHB1Periph_GPIOI };
API_GPIO_type_t PI14 = { GPIOI, GPIO_Pin_14, RCC_AHB1Periph_GPIOI };
API_GPIO_type_t PI15 = { GPIOI, GPIO_Pin_15, RCC_AHB1Periph_GPIOI };

/* Private function prototypes -----------------------------------------------*/
uint8_t API_getDI(API_GPIO_type_t* portpin);
uint16_t API_getDI_port(GPIO_TypeDef* GPIOx);

void API_setDO(API_GPIO_type_t* portpin, BitAction bit_val);
void API_setDO_port(GPIO_TypeDef* GPIOx, uint16_t port_val);

void API_toggleDO(API_GPIO_type_t* portpin);
void API_toggleDO_port(GPIO_TypeDef* GPIOx);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Read the logical level low or high at the selected portpin.
  * @param  portpin: can be PA0..PA15, ..., PI0..PI15
  * @retval GPIO input data port value (0, 1)
  */
uint8_t API_getDI(API_GPIO_type_t* portpin){
  GPIO_InitTypeDef  GPIO_InitStructure;
  
  /* API_GPIOx Periph clock enable */
  RCC_AHB1PeriphClockCmd(portpin->RCC_AHB1Periph_GPIOx, ENABLE);

  /* configure portpin to input mode */
  GPIO_InitStructure.GPIO_Pin   = portpin->GPIO_Pin_x;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Init(portpin->GPIOx, &GPIO_InitStructure);

  /* read data */
  return (uint8_t)GPIO_ReadInputDataBit(portpin->GPIOx, portpin->GPIO_Pin_x);
}

/**
  * @brief  Read the logical level low or high at the selected port.
  * @param  API_GPIOx: can be GPIOA..GPIOI
  * @retval GPIO input data port value (0x00..0xFF)
  */
uint16_t API_getDI_port(GPIO_TypeDef* GPIOx){
  GPIO_InitTypeDef  GPIO_InitStructure;
  
  /* GPIOx Periph clock enable */
  if(GPIOx == GPIOA)      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  else if(GPIOx == GPIOB) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  else if(GPIOx == GPIOC) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
  else if(GPIOx == GPIOD) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
  else if(GPIOx == GPIOE) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
  else if(GPIOx == GPIOF) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);
  else if(GPIOx == GPIOG) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG, ENABLE);
  else if(GPIOx == GPIOH) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOH, ENABLE);
  else if(GPIOx == GPIOI) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOI, ENABLE);

  /* configure portpin to output pushpull mode */
  GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_All;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Init(GPIOx, &GPIO_InitStructure);

  /* read data */
  return (uint16_t)GPIO_ReadInputData(GPIOx);
}

/**
  * @brief  Set the logical level low or high at the selected portpin.
  * @param  portpin: can be PA0..PA15, ..., PI0..PI15
  * @param  bit_val: can be Bit_RESET or Bit_SET to select low or high
  * @retval none
  */
void API_setDO(API_GPIO_type_t* portpin, BitAction bit_val){
  GPIO_InitTypeDef  GPIO_InitStructure;
  
  /* API_GPIOx Periph clock enable */
  RCC_AHB1PeriphClockCmd(portpin->RCC_AHB1Periph_GPIOx, ENABLE);

  /* configure portpin to output pushpull mode */
  GPIO_InitStructure.GPIO_Pin   = portpin->GPIO_Pin_x;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Init(portpin->GPIOx, &GPIO_InitStructure);

  /* set data */
  GPIO_WriteBit(portpin->GPIOx, portpin->GPIO_Pin_x, bit_val);
}

/**
  * @brief  Set the logical level low or high at the selected port.
  * @param  API_GPIOx: can be GPIOA..GPIOI
  * @param  port_val: can be 0x00 to 0xFF
  * @retval none
  */
void API_setDO_port(GPIO_TypeDef* GPIOx, uint16_t port_val){
  GPIO_InitTypeDef  GPIO_InitStructure;
  
  /* GPIOx Periph clock enable */
  if(GPIOx == GPIOA)      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  else if(GPIOx == GPIOB) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  else if(GPIOx == GPIOC) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
  else if(GPIOx == GPIOD) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
  else if(GPIOx == GPIOE) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
  else if(GPIOx == GPIOF) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);
  else if(GPIOx == GPIOG) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG, ENABLE);
  else if(GPIOx == GPIOH) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOH, ENABLE);
  else if(GPIOx == GPIOI) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOI, ENABLE);

  /* configure portpin to output pushpull mode */
  GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_All;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Init(GPIOx, &GPIO_InitStructure);

  /* set data */
  GPIO_Write(GPIOx, port_val);
}

/**
  * @brief  Toggles the logic state of the portpin.
  * @param  portpin: can be PA0..PA15, ..., PI0..PI15
  * @retval none
  */
void API_toggleDO(API_GPIO_type_t* portpin){
  GPIO_InitTypeDef  GPIO_InitStructure;
  
  /* API_GPIOx Periph clock enable */
  RCC_AHB1PeriphClockCmd(portpin->RCC_AHB1Periph_GPIOx, ENABLE);

  /* configure portpin to output pushpull mode */
  GPIO_InitStructure.GPIO_Pin   = portpin->GPIO_Pin_x;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Init(portpin->GPIOx, &GPIO_InitStructure);

  /* set data */
  GPIO_ToggleBits(portpin->GPIOx, portpin->GPIO_Pin_x);
}

/**
  * @brief  Toggles the logic state of the port.
  * @param  API_GPIOx: can be GPIOA..GPIOI
  * @retval none
  */
void API_toggleDO_port(GPIO_TypeDef* GPIOx){
  GPIO_InitTypeDef  GPIO_InitStructure;
  
  uint16_t port_val = 0;
  
  /* GPIOx Periph clock enable */
  if(GPIOx == GPIOA)      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  else if(GPIOx == GPIOB) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  else if(GPIOx == GPIOC) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
  else if(GPIOx == GPIOD) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
  else if(GPIOx == GPIOE) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
  else if(GPIOx == GPIOF) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);
  else if(GPIOx == GPIOG) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG, ENABLE);
  else if(GPIOx == GPIOH) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOH, ENABLE);
  else if(GPIOx == GPIOI) RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOI, ENABLE);

  /* configure portpin to output pushpull mode */
  GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_All;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Init(GPIOx, &GPIO_InitStructure);
  
  /* read data */
  port_val = GPIO_ReadOutputData(GPIOx);

  /* set data */
  GPIO_Write(GPIOx, ~port_val);
}

#endif // __USING_GPIO
