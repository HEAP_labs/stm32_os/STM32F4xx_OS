/**
 * @file    ./API/src/Flash.c
 * @author  Andreas Hirtenlehner
 * @brief   API for Flash
 */
  
#include "PerUsings.h"
#ifdef __USING_FLASH

/* Includes ------------------------------------------------------------------*/
#include "Flash.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
FLASH_Status API_FLASH_write(uint32_t startAddr, uint32_t endAddr, uint8_t *pData);
FLASH_Status API_FLASH_read(uint32_t startAddr, uint32_t endAddr, uint8_t *pData);
FLASH_Status API_FLASH_clear(uint32_t startAddr, uint32_t endAddr, uint8_t data);
uint32_t API_FLASH_check(uint32_t startAddr, uint32_t endAddr, uint8_t *pData);
FLASH_Status API_FLASH_erase_sector(uint16_t startSector, uint16_t endSector);
uint32_t API_FLASH_get_sector(uint32_t Address);
uint32_t API_FLASH_get_next_free_address(uint32_t startAddr, uint32_t endAddr);

/* Private functions ---------------------------------------------------------*/
FLASH_Status API_FLASH_write(uint32_t startAddr, uint32_t endAddr, uint8_t *pData)
{
  FLASH_Status state = FLASH_BUSY;
  uint32_t addr;
  uint32_t i = 0;

  FLASH_Unlock();

  for(addr = startAddr; addr <= endAddr; addr++)
  {
    state = FLASH_ProgramByte(addr, pData[i]);
    i++;
    if(state != FLASH_COMPLETE) break;
  }

  FLASH_Lock();

  return state;
}

FLASH_Status API_FLASH_read(uint32_t startAddr, uint32_t endAddr, uint8_t *pData)
{
  FLASH_Status state = FLASH_BUSY;

  return state;
}

FLASH_Status API_FLASH_clear(uint32_t startAddr, uint32_t endAddr, uint8_t data)
{
  FLASH_Status state = FLASH_BUSY;
  uint32_t addr;
  uint32_t i = 0;

  FLASH_Unlock();

  for(addr = startAddr; addr <= endAddr; addr++)
  {
    state = FLASH_ProgramByte(addr, data);
    i++;
    if(state != FLASH_COMPLETE) break;
  }

  FLASH_Lock();

  return state;
}

uint32_t API_FLASH_check(uint32_t startAddr, uint32_t endAddr, uint8_t *pData)
{
  __IO uint32_t MemoryProgramStatus = 0;
  __IO uint32_t data = 0;
  uint32_t addr = startAddr;
  uint32_t i = 0;

  FLASH_Unlock();

  while (addr <= endAddr)
  {
    data = *(__IO uint8_t*)addr;

    if (data != pData[i]) {
        MemoryProgramStatus++;
        FLASH_ProgramByte(addr, 0x00);
    }

    addr++;
    i++;
  }

  FLASH_Lock();

  return MemoryProgramStatus;
}

FLASH_Status API_FLASH_erase_sector(uint16_t startSector, uint16_t endSector)
{
  uint8_t i = 0;
  FLASH_Status status;

  FLASH_Unlock();
  FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);

  for (i = startSector; i < endSector; i += 8)
  {
    status = FLASH_EraseSector(i, VoltageRange_3);
    if (status != FLASH_COMPLETE)    { return status; }
  }

  FLASH_Lock();

  return FLASH_COMPLETE;
}

/**
  * @brief  Gets the sector of a given address
  * @param  Address: Flash address
  * @retval The sector of a given address
  */
uint32_t API_FLASH_get_sector(uint32_t Address)
{
  uint32_t sector = 0;

  if((Address < ADDR_FLASH_SECTOR_1) && (Address >= ADDR_FLASH_SECTOR_0))         { sector = FLASH_Sector_0;  }
  else if((Address < ADDR_FLASH_SECTOR_2) && (Address >= ADDR_FLASH_SECTOR_1))    { sector = FLASH_Sector_1;  }
  else if((Address < ADDR_FLASH_SECTOR_3) && (Address >= ADDR_FLASH_SECTOR_2))    { sector = FLASH_Sector_2;  }
  else if((Address < ADDR_FLASH_SECTOR_4) && (Address >= ADDR_FLASH_SECTOR_3))    { sector = FLASH_Sector_3;  }
  else if((Address < ADDR_FLASH_SECTOR_5) && (Address >= ADDR_FLASH_SECTOR_4))    { sector = FLASH_Sector_4;  }
  else if((Address < ADDR_FLASH_SECTOR_6) && (Address >= ADDR_FLASH_SECTOR_5))    { sector = FLASH_Sector_5;  }
  else if((Address < ADDR_FLASH_SECTOR_7) && (Address >= ADDR_FLASH_SECTOR_6))    { sector = FLASH_Sector_6;  }
  else if((Address < ADDR_FLASH_SECTOR_8) && (Address >= ADDR_FLASH_SECTOR_7))    { sector = FLASH_Sector_7;  }
  else if((Address < ADDR_FLASH_SECTOR_9) && (Address >= ADDR_FLASH_SECTOR_8))    { sector = FLASH_Sector_8;  }
  else if((Address < ADDR_FLASH_SECTOR_10) && (Address >= ADDR_FLASH_SECTOR_9))   { sector = FLASH_Sector_9;  }
  else if((Address < ADDR_FLASH_SECTOR_11) && (Address >= ADDR_FLASH_SECTOR_10))  { sector = FLASH_Sector_10; }
  else                                                                            { sector = FLASH_Sector_11; }

  return sector;
}

uint32_t API_FLASH_get_next_free_address(uint32_t startAddr, uint32_t endAddr)
{
  uint32_t addr;

  for(addr = startAddr; addr < endAddr - 4; addr = addr + 4)
  {
    uint32_t data = *(__IO uint32_t*)addr;

    if((data & 0xFF000000) == 0xFF000000)      return addr;
    else if((data & 0x00FF0000) == 0x00FF0000) return addr + 1;
    else if((data & 0x0000FF00) == 0x0000FF00) return addr + 2;
    else if((data & 0x000000FF) == 0x000000FF) return addr + 3;
  }

  return 0;
}

#endif // __USING_FLASH
