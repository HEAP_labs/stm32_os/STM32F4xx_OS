/**
 * @file    ./API/src/Timer.c
 * @author  Andreas Hirtenlehner, Gerald Ebmer
 * @brief   API for Timer
 */
  
#include "PerUsings.h"
#ifdef __USING_TIMER

/* Includes ------------------------------------------------------------------*/
#include "Timer.h"

/* Private typedef -----------------------------------------------------------*/
TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
NVIC_InitTypeDef NVIC_InitStructure;

/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
API_TIM_type_t API_TIM1 =  { TIM1,  RCC_APB2Periph_TIM1,  GPIO_AF_TIM1,  TIM1_CC_IRQn            };
API_TIM_type_t API_TIM2 =  { TIM2,  RCC_APB1Periph_TIM2,  GPIO_AF_TIM2,  TIM2_IRQn               };
API_TIM_type_t API_TIM3 =  { TIM3,  RCC_APB1Periph_TIM3,  GPIO_AF_TIM3,  TIM3_IRQn               };
API_TIM_type_t API_TIM4 =  { TIM4,  RCC_APB1Periph_TIM4,  GPIO_AF_TIM4,  TIM4_IRQn               };
API_TIM_type_t API_TIM5 =  { TIM5,  RCC_APB1Periph_TIM5,  GPIO_AF_TIM5,  TIM5_IRQn               };
API_TIM_type_t API_TIM6 =  { TIM6,  RCC_APB1Periph_TIM6,  0xFF        ,  TIM6_DAC_IRQn           };
API_TIM_type_t API_TIM7 =  { TIM7,  RCC_APB1Periph_TIM7,  0xFF        ,  TIM7_IRQn               };
API_TIM_type_t API_TIM8 =  { TIM8,  RCC_APB2Periph_TIM8,  GPIO_AF_TIM8,  TIM8_CC_IRQn            };
API_TIM_type_t API_TIM9 =  { TIM9,  RCC_APB2Periph_TIM9,  GPIO_AF_TIM9,  TIM1_BRK_TIM9_IRQn      };
API_TIM_type_t API_TIM10 = { TIM10, RCC_APB2Periph_TIM10, GPIO_AF_TIM10, TIM1_UP_TIM10_IRQn      };
API_TIM_type_t API_TIM11 = { TIM11, RCC_APB2Periph_TIM11, GPIO_AF_TIM11, TIM1_TRG_COM_TIM11_IRQn };
API_TIM_type_t API_TIM12 = { TIM12, RCC_APB1Periph_TIM12, GPIO_AF_TIM12, TIM8_BRK_TIM12_IRQn     };
API_TIM_type_t API_TIM13 = { TIM13, RCC_APB1Periph_TIM13, GPIO_AF_TIM13, TIM8_UP_TIM13_IRQn      };
API_TIM_type_t API_TIM14 = { TIM14, RCC_APB1Periph_TIM14, GPIO_AF_TIM14, TIM8_TRG_COM_TIM14_IRQn };

/* Private function prototypes -----------------------------------------------*/
void API_start_taskclass(API_TIM_type_t* API_TIMx, double t_period_s, uint8_t priority);
void API_PWM_start(API_TIM_type_t* API_TIMx, uint32_t CHx, API_GPIO_type_t* portpin, double t_period_s, double dutycycle, uint16_t TIM_OCPolarity);
void API_PWM_start_complementary(API_TIM_type_t* API_TIMx, API_GPIO_type_t* portpin, double t_period_s, double dutycycle, uint8_t DeadTime_ClkCycles, uint16_t TIM_OCPolarity);
void API_PWM_set_dutycycle(API_TIM_type_t* API_TIMx, uint8_t CHx, double dutycycle);
static void timer_clock_enable(API_TIM_type_t* API_TIMx);
static uint32_t timer_get_clock_frequency(API_TIM_type_t* API_TIMx);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Start a new Taskclass
  * @param  Timer: can be API_TIM1 .. API_TIM14
  * @param  t_period_s: Time for one period
  * @param  priority: Interrupt priority (0..15)
  * @retval none
  */
void API_start_taskclass(API_TIM_type_t* API_TIMx, double t_period_s, uint8_t priority)
{
  uint32_t period_timer_ticks = 0;
  uint32_t prescaler = 0;
  uint32_t timer_clock = 0;

  /* TIMx clock enable */
  timer_clock_enable(API_TIMx);
  
  timer_clock = timer_get_clock_frequency(API_TIMx);
  
  /* Prescaler berechnen */
  if(API_TIMx->TIMx == API_TIM2.TIMx){
    prescaler = (uint16_t)((timer_clock) * t_period_s / 0x100000000);
  }
  else if(API_TIMx->TIMx == API_TIM5.TIMx){
    prescaler = (uint16_t)((timer_clock) * t_period_s / 0x100000000);
  }
  else{
    prescaler = (uint16_t)((timer_clock) * t_period_s / 0x10000);
  }

  period_timer_ticks = (timer_clock * t_period_s / ((prescaler + 1))); 

  /* Time Base configuration */
  TIM_TimeBaseStructure.TIM_Prescaler = prescaler;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = period_timer_ticks;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;

  TIM_TimeBaseInit(API_TIMx->TIMx, &TIM_TimeBaseStructure);

  /* TIMx counter enable */
  TIM_Cmd(API_TIMx->TIMx, ENABLE);

  /* Interrupt konfigurieren */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

  NVIC_InitStructure.NVIC_IRQChannel = API_TIMx->IRQnx;                // Interrupt Source ausw�hlen
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = priority; // pre-emption Priorit�t ausw�hlen
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;               // sub priority ausw�hlen
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;                  // enablen
  NVIC_Init(&NVIC_InitStructure);                                  // Parameter �bernehmen

  TIM_ITConfig(API_TIMx->TIMx, TIM_IT_Update, ENABLE);
}

/**
  * @brief  Start a new PWM Signal on a GPIO
  * @param  Timer: can be API_TIM1 .. API_TIM5, API_TIM8 .. API_TIM14
  * @param  CHx: Timer-channel (can be CH1, CH2, CH3 or CH4)
  * @param  portpin: Output Pin for the PWM, can be PA0..PA15, ..., PI0..PI15 (depends on Timer and Timer-channel)
  * @param  t_period_s: Time for one period
  * @param  dutycycle: (SystemCoreClock / 2) * t_period_se_s / (Prescaler + 1
  * @param  TIM_OCPolarity: can be TIM_OCPolarity_High or TIM_OCPolarity_Low
  * @retval none
  */
void API_PWM_start(API_TIM_type_t* API_TIMx, uint32_t CHx, API_GPIO_type_t* portpin, double t_period_s, double duty_cycle, uint16_t TIM_OCPolarity)
{
  TIM_OCInitTypeDef TIM_OCInitStructure;
  GPIO_InitTypeDef  GPIO_InitStructure;

  uint32_t pulse_timer_ticks  = 0;
  uint32_t period_timer_ticks = 0;
  uint16_t prescaler         = 0;
  uint16_t pin_source         = 0;
  uint32_t timer_clock        = 0;

  while((portpin->GPIO_Pin_x >> pin_source) > 0x01) pin_source++;

  /* TIMx clock enable */
  timer_clock_enable(API_TIMx);
  
  timer_clock = timer_get_clock_frequency(API_TIMx);

  /* Prescaler berechnen */
  if(API_TIMx->TIMx == API_TIM2.TIMx){
    prescaler = (uint16_t)((timer_clock) * t_period_s / 0x100000000);
  }
  else if(API_TIMx->TIMx == API_TIM5.TIMx){
    prescaler = (uint16_t)((timer_clock) * t_period_s / 0x100000000);
  }
  else{
    prescaler = (uint16_t)((timer_clock) * t_period_s / 0x10000);
  }

  period_timer_ticks = (timer_clock * t_period_s / ((prescaler + 1))); 
  pulse_timer_ticks  = period_timer_ticks * duty_cycle;

  /* API_GPIOx Periph clock enable */
  RCC_AHB1PeriphClockCmd(portpin->RCC_AHB1Periph_GPIOx, ENABLE);

  /* Set AF Pin */
  GPIO_PinAFConfig(portpin->GPIOx, pin_source, API_TIMx->GPIO_AF_TIMx);

  GPIO_InitStructure.GPIO_Pin = portpin->GPIO_Pin_x;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_Init(portpin->GPIOx, &GPIO_InitStructure);
  
  /* Time Base configuration */
  TIM_TimeBaseStructure.TIM_Prescaler = prescaler;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = period_timer_ticks;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;

  TIM_TimeBaseInit(API_TIMx->TIMx, &TIM_TimeBaseStructure);
  
  API_TIMx->period_timer_ticks = period_timer_ticks;

  /* TIMx counter enable */
  TIM_Cmd(API_TIMx->TIMx, ENABLE);


  /* PWM Mode configuration */
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = pulse_timer_ticks;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity;

  if(CHx == 0x01)      { TIM_OC1Init(API_TIMx->TIMx, &TIM_OCInitStructure); TIM_OC1PreloadConfig(API_TIMx->TIMx, TIM_OCPreload_Enable); }
  else if(CHx == 0x02) { TIM_OC2Init(API_TIMx->TIMx, &TIM_OCInitStructure); TIM_OC2PreloadConfig(API_TIMx->TIMx, TIM_OCPreload_Enable); }
  else if(CHx == 0x04) { TIM_OC3Init(API_TIMx->TIMx, &TIM_OCInitStructure); TIM_OC3PreloadConfig(API_TIMx->TIMx, TIM_OCPreload_Enable); }
  else if(CHx == 0x08) { TIM_OC4Init(API_TIMx->TIMx, &TIM_OCInitStructure); TIM_OC4PreloadConfig(API_TIMx->TIMx, TIM_OCPreload_Enable); }
  //else;
}

/** Version 0.1 --no release--
  * Generates 6 Phase PWM. Eache Phase is shifted by 2/3*pi (120�)
  * Execution Time: ~25�s @ 168MHz
  * @brief  Start a 6 phase PWM
  * @param  Timer: can be API_TIM1  or API_TIM8
  * @param  portpin set fix in V0.1
  *         Ch1     PA8
  *         Ch1N    PB13
  *         BKIN    PB12
  * @param  t_period_s: Time for one period
  * @param  dutycycle: (SystemCoreClock / 2) * t_period_se_s / (Prescaler + 1
  * @param  DeadTime:   DeadTime in Clockcycles
  * @param  TIM_OCPolarity: can be TIM_OCPolarity_High or TIM_OCPolarity_Low; default use High (during Deadtime both channels are logic "0")
  * @retval none
  */
void API_PWM_start_complementary(API_TIM_type_t* API_TIMx, API_GPIO_type_t* portpin, double t_period_s, double dutycycle, uint8_t DeadTime_ClkCycles, uint16_t TIM_OCPolarity)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  TIM_OCInitTypeDef  TIM_OCInitStructure;
  TIM_BDTRInitTypeDef TIM_BDTRInitStructure;
  uint16_t Prescaler      = (uint16_t)((SystemCoreClock / 2.0) * t_period_s / 0x10000);
  uint16_t TimerPeriod    = (SystemCoreClock * t_period_s / ((Prescaler + 1.0) * 2.0)) - 1.0;  /* Compute the value to be set in ARR register to generate signal frequency at 17.57 Khz */
  uint16_t Channel1Pulse  = (uint16_t)(TimerPeriod * dutycycle);                        /* Compute CCR1 value to generate a duty cycle at 50% for channel 1 */

  /*** GPIO Configuration ***/
  /* GPIOA and GPIOB clocks enable */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB, ENABLE);

  /* GPIOA Configuration: Channel 1 and 3 as alternate function push-pull */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* GPIOB Configuration: BKIN, Channel 1N, 2N and 3N as alternate function push-pull */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  /* Connect TIM pins to AF1 */
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource8, GPIO_AF_TIM1);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource12, GPIO_AF_TIM1);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_TIM1);

  /*** Timer Configuration ***/
  /* TIM1 clock enable */
  if(API_TIMx->TIMx == API_TIM1.TIMx)       RCC_APB2PeriphClockCmd(API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else if(API_TIMx->TIMx == API_TIM8.TIMx)  RCC_APB2PeriphClockCmd(API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else  return; // falscher Timer gew�hlt

  /* Time Base configuration */
  TIM_TimeBaseStructure.TIM_Prescaler = Prescaler;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = TimerPeriod;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;

  TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);

  /* Channel 1 Configuration in PWM mode */
  TIM_OCInitStructure.TIM_OCMode        = TIM_OCMode_PWM2;
  TIM_OCInitStructure.TIM_OutputState   = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_OutputNState  = TIM_OutputNState_Enable;
  TIM_OCInitStructure.TIM_Pulse         = Channel1Pulse;
  TIM_OCInitStructure.TIM_OCPolarity    = TIM_OCPolarity;
  TIM_OCInitStructure.TIM_OCNPolarity   = TIM_OCPolarity;
  TIM_OCInitStructure.TIM_OCIdleState   = TIM_OCIdleState_Set;
  TIM_OCInitStructure.TIM_OCNIdleState  = TIM_OCIdleState_Reset;

  TIM_OC1Init(TIM1, &TIM_OCInitStructure);

  /* Automatic Output enable, Break, dead time and lock configuration*/
  TIM_BDTRInitStructure.TIM_OSSRState       = TIM_OSSRState_Enable;
  TIM_BDTRInitStructure.TIM_OSSIState       = TIM_OSSIState_Enable;
  TIM_BDTRInitStructure.TIM_LOCKLevel       = TIM_LOCKLevel_1;
  TIM_BDTRInitStructure.TIM_DeadTime        = DeadTime_ClkCycles;
  TIM_BDTRInitStructure.TIM_Break           = TIM_Break_Enable;
  TIM_BDTRInitStructure.TIM_BreakPolarity   = TIM_BreakPolarity_High;
  TIM_BDTRInitStructure.TIM_AutomaticOutput = TIM_AutomaticOutput_Enable;

  TIM_BDTRConfig(TIM1, &TIM_BDTRInitStructure);

  /* TIM1 counter enable */
  TIM_Cmd(TIM1, ENABLE);

  /* Main Output Enable */
  TIM_CtrlPWMOutputs(TIM1, ENABLE);

  /* Save PWM Settings */
  API_TIMx->period_timer_ticks = TimerPeriod;
}

void API_PWM_set_dutycycle(API_TIM_type_t* API_TIMx, uint8_t CHx, double dutycycle)
{
  uint16_t TimerPeriod   = API_TIMx->period_timer_ticks;
  uint16_t ChannelPulse  = (uint16_t)(TimerPeriod * dutycycle);                        

  /*** Timer Configuration ***/

  /* Channel x Configuration in PWM mode */
  if(CHx == CH1) TIM_SetCompare1(API_TIMx->TIMx, ChannelPulse);
  else if(CHx == CH2) TIM_SetCompare2(API_TIMx->TIMx, ChannelPulse);
  else if(CHx == CH3) TIM_SetCompare3(API_TIMx->TIMx, ChannelPulse);
  else if(CHx == CH4) TIM_SetCompare4(API_TIMx->TIMx, ChannelPulse);
}

/**
  * @brief  enable clock of TIMx
  * @param  API_TIMx: selected API_TIMx
  * @retval none
  */
void timer_clock_enable(API_TIM_type_t* API_TIMx)
{
  if(API_TIMx->TIMx == API_TIM1.TIMx)       RCC_APB2PeriphClockCmd(API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else if(API_TIMx->TIMx == API_TIM8.TIMx)  RCC_APB2PeriphClockCmd(API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else if(API_TIMx->TIMx == API_TIM9.TIMx)  RCC_APB2PeriphClockCmd(API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else if(API_TIMx->TIMx == API_TIM10.TIMx) RCC_APB2PeriphClockCmd(API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else if(API_TIMx->TIMx == API_TIM11.TIMx) RCC_APB2PeriphClockCmd(API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else                                      RCC_APB1PeriphClockCmd(API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
}

/**
  * @brief  read clock frequencie of TIMx
  * @param  API_TIMx: selected API_TIMx
  * @retval clock frequency
  */
uint32_t timer_get_clock_frequency(API_TIM_type_t* API_TIMx)
{
  RCC_ClocksTypeDef RCC_Clocks;
  
  RCC_GetClocksFreq(&RCC_Clocks);
  
  if(API_TIMx->TIMx == API_TIM1.TIMx)       return RCC_Clocks.PCLK2_Frequency * 2.0;
  else if(API_TIMx->TIMx == API_TIM2.TIMx)  return RCC_Clocks.PCLK1_Frequency * 2.0; 
  else if(API_TIMx->TIMx == API_TIM3.TIMx)  return RCC_Clocks.PCLK1_Frequency * 2.0; 
  else if(API_TIMx->TIMx == API_TIM4.TIMx)  return RCC_Clocks.PCLK1_Frequency * 2.0; 
  else if(API_TIMx->TIMx == API_TIM5.TIMx)  return RCC_Clocks.PCLK1_Frequency * 2.0; 
  else if(API_TIMx->TIMx == API_TIM6.TIMx)  return RCC_Clocks.PCLK1_Frequency * 2.0; 
  else if(API_TIMx->TIMx == API_TIM7.TIMx)  return RCC_Clocks.PCLK1_Frequency * 2.0;
  else if(API_TIMx->TIMx == API_TIM8.TIMx)  return RCC_Clocks.PCLK2_Frequency * 2.0; 
  else if(API_TIMx->TIMx == API_TIM9.TIMx)  return RCC_Clocks.PCLK2_Frequency * 2.0; 
  else if(API_TIMx->TIMx == API_TIM10.TIMx) return RCC_Clocks.PCLK2_Frequency * 2.0; 
  else if(API_TIMx->TIMx == API_TIM11.TIMx) return RCC_Clocks.PCLK2_Frequency * 2.0; 
  else if(API_TIMx->TIMx == API_TIM12.TIMx) return RCC_Clocks.PCLK1_Frequency * 2.0; 
  else if(API_TIMx->TIMx == API_TIM13.TIMx) return RCC_Clocks.PCLK1_Frequency * 2.0; 
  else if(API_TIMx->TIMx == API_TIM14.TIMx) return RCC_Clocks.PCLK1_Frequency * 2.0;  
  else                                      return 0;
}

#endif // __USING_TIMER
