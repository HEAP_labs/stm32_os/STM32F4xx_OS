/**
 * @file    ./API/src/USART.c
 * @author  Andreas Hirtenlehner, Gerald Ebmer
 * @brief   API for USART
 */
 
#include "PerUsings.h"
#ifdef __USING_USART

/* Includes ------------------------------------------------------------------*/
#include "USART.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
API_USART_type_t API_USART1 = { USART1, RCC_APB2Periph_USART1, GPIO_AF_USART1, USART1_IRQn, DMA2, DMA2_Stream7, DMA2_Stream2, DMA_Channel_4, DMA2_Stream7_IRQn, DMA2_Stream2_IRQn, 0, 0 };
API_USART_type_t API_USART2 = { USART2, RCC_APB1Periph_USART2, GPIO_AF_USART2, USART2_IRQn, DMA1, DMA1_Stream6, DMA1_Stream5, DMA_Channel_4, DMA1_Stream6_IRQn, DMA1_Stream5_IRQn, 0, 0 };
API_USART_type_t API_USART3 = { USART3, RCC_APB1Periph_USART3, GPIO_AF_USART3, USART3_IRQn, DMA1, DMA1_Stream3, DMA1_Stream1, DMA_Channel_4, DMA1_Stream3_IRQn, DMA1_Stream1_IRQn, 0, 0 };
API_USART_type_t API_USART4 = { UART4,  RCC_APB1Periph_UART4,  GPIO_AF_UART4,  UART4_IRQn,  DMA1, DMA1_Stream4, DMA1_Stream2, DMA_Channel_4, DMA1_Stream4_IRQn, DMA1_Stream2_IRQn, 0, 0 };
API_USART_type_t API_USART5 = { UART5,  RCC_APB1Periph_UART5,  GPIO_AF_UART5,  UART5_IRQn,  DMA1, DMA1_Stream7, DMA1_Stream0, DMA_Channel_4, DMA1_Stream7_IRQn, DMA1_Stream0_IRQn, 0, 0 };
API_USART_type_t API_USART6 = { USART6, RCC_APB2Periph_USART6, GPIO_AF_USART6, USART6_IRQn, DMA2, DMA2_Stream6, DMA2_Stream1, DMA_Channel_5, DMA2_Stream6_IRQn, DMA2_Stream1_IRQn, 0, 0 };

/* Private function prototypes -----------------------------------------------*/
void API_USART_init(API_USART_type_t* USART, API_GPIO_type_t* Portpin_Tx, API_GPIO_type_t* Portpin_Rx, uint32_t BaudRate, uint8_t priority);
void API_USART_DMA_init(API_USART_type_t* USART, uint8_t* p_tx_data, uint8_t* p_rx_data, uint8_t tx_priority, uint8_t rx_priority);
void API_USART_send_byte(API_USART_type_t* USART, uint8_t SendByte);
void API_USART_send_data(API_USART_type_t* USART, const char *ptr, long len);
void API_USART_send_string(API_USART_type_t* USART, const char* FormatString, ...);
void API_USART_DMA_send(API_USART_type_t* USART, uint16_t tx_length);
void API_USART_DMA_receive(API_USART_type_t* USART, uint16_t rx_length);
uint8_t API_USART_get_byte_from_buffer(API_USART_type_t* API_USARTx);

/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Send a single Byte using USART_x
  * @param  USART: can be USART_1..USART_6
  * @param  Portpin: Tx pin for USART, can be PA0..PA15, ..., PI0..PI15 (depends on selected USART)
  * @param  BaudRate: Baudrate for the selected USART
  * @param  priority: Preemption Priority for selected USART
  * @note   for STM32F407: USART_4 and USART_5 are UARTs
  * @retval none
  */
void API_USART_init(API_USART_type_t* USART, API_GPIO_type_t* Portpin_Tx, API_GPIO_type_t* Portpin_Rx, uint32_t BaudRate, uint8_t priority)
{
    USART_InitTypeDef  USART_InitStructure;
    GPIO_InitTypeDef   GPIO_InitStructure;
    NVIC_InitTypeDef   NVIC_InitStructure;

    uint16_t PinSource_Tx = 0;
    uint16_t PinSource_Rx = 0;

    USART_DeInit(USART->USARTx);

    while((Portpin_Tx->GPIO_Pin_x >> PinSource_Tx) > 0x01) PinSource_Tx++;
    while((Portpin_Rx->GPIO_Pin_x >> PinSource_Rx) > 0x01) PinSource_Rx++;

    /* USART_x Periph clock enable */
    if(USART->USARTx == API_USART1.USARTx)      RCC_APB2PeriphClockCmd(USART->RCC_APBxPeriph_USARTx, ENABLE);
    else if(USART->USARTx == API_USART6.USARTx) RCC_APB2PeriphClockCmd(USART->RCC_APBxPeriph_USARTx, ENABLE);
    else                                    RCC_APB1PeriphClockCmd(USART->RCC_APBxPeriph_USARTx, ENABLE);

    /* GPIOx Periph clock enable */
    RCC_AHB1PeriphClockCmd(Portpin_Tx->RCC_AHB1Periph_GPIOx, ENABLE);
    RCC_AHB1PeriphClockCmd(Portpin_Rx->RCC_AHB1Periph_GPIOx, ENABLE);

    /* Set AF Pin */
    GPIO_PinAFConfig(Portpin_Tx->GPIOx, PinSource_Tx, USART->GPIO_AF_USARTx);
    GPIO_PinAFConfig(Portpin_Rx->GPIOx, PinSource_Rx, USART->GPIO_AF_USARTx);

    GPIO_InitStructure.GPIO_Pin = Portpin_Tx->GPIO_Pin_x;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_Init(Portpin_Tx->GPIOx, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = Portpin_Rx->GPIO_Pin_x;
    GPIO_Init(Portpin_Rx->GPIOx, &GPIO_InitStructure);

    /* configure USART */
    USART_InitStructure.USART_BaudRate = BaudRate;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_Init(USART->USARTx, &USART_InitStructure);

    /* configure NVIC */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

    NVIC_InitStructure.NVIC_IRQChannel = USART->IRQnx;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = priority;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    /* Interrupt enable */
    USART_ITConfig(USART->USARTx, USART_IT_RXNE, ENABLE);
    USART_ITConfig(USART->USARTx, USART_IT_TC, ENABLE);

    /* USART enable */
    USART_Cmd(USART->USARTx,ENABLE);

    USART->isInit = 1;
}

void API_USART_DMA_init(API_USART_type_t* USART, uint8_t* p_tx_data, uint8_t* p_rx_data, uint8_t tx_priority, uint8_t rx_priority)
{
    NVIC_InitTypeDef   NVIC_InitStructure;
    DMA_InitTypeDef    DMA_InitStructure;

    if(USART->DMAx == DMA1) { RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE); }
    else                    { RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE); }

    /* configure NVIC */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

    NVIC_InitStructure.NVIC_IRQChannel = USART->DMAx_Streamx_IRQn_Tx;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = tx_priority;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = USART->DMAx_Streamx_IRQn_Rx;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = rx_priority;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    /* Interrupt enable */
    USART_ITConfig(USART->USARTx, USART_IT_RXNE, ENABLE);
    USART_ITConfig(USART->USARTx, USART_IT_TC, ENABLE);

    DMA_DeInit(USART->DMAx_Streamx_Tx);
    DMA_InitStructure.DMA_BufferSize = 0;
    DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
    DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
    DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single ;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_PeripheralBaseAddr =(uint32_t) (&(USART->USARTx->DR));
    DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;

    DMA_InitStructure.DMA_Channel = USART->DMA_Channel_x;
    DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
    DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)p_tx_data;
    DMA_Init(USART->DMAx_Streamx_Tx, &DMA_InitStructure);

    DMA_DeInit(USART->DMAx_Streamx_Rx);
    DMA_InitStructure.DMA_BufferSize = 0;
    DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
    DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
    DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single ;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_PeripheralBaseAddr =(uint32_t) (&(USART->USARTx->DR));
    DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;

    DMA_InitStructure.DMA_Channel = USART->DMA_Channel_x;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
    DMA_InitStructure.DMA_Memory0BaseAddr =(uint32_t)p_rx_data;
    DMA_Init(USART->DMAx_Streamx_Rx, &DMA_InitStructure);

    // enable DMA request
    USART_DMACmd(USART->USARTx, USART_DMAReq_Rx, ENABLE);
    USART_DMACmd(USART->USARTx, USART_DMAReq_Tx, ENABLE);

    // DMA-RX Transfer-Complete Interrupt enable
    DMA_ITConfig(USART->DMAx_Streamx_Rx, DMA_IT_TC, ENABLE);
    DMA_ITConfig(USART->DMAx_Streamx_Tx, DMA_IT_TC, ENABLE);

    // DMA-RX enable
    DMA_Cmd(USART->DMAx_Streamx_Rx,ENABLE);
}

/**
  * @brief  Send a single Byte using USART_x
  * @param  USART: can be USART_1..USART_6
  * @param  SendByte: Data to send
  * @note   for STM32F407: USART_4 and USART_5 are UARTs
  * @retval none
  */
void API_USART_send_byte(API_USART_type_t* USART, uint8_t SendByte)
{
  USART->busy = 1;
  USART_SendData(USART->USARTx, SendByte);
  while(USART_GetFlagStatus(USART->USARTx, USART_FLAG_TC) == RESET);
  USART->busy = 0;
}

/**
  * @brief  Send a String using USART_x
  * @param  USART: can be USART_1..USART_6
  * @param  All other parameters like printf()
  * @note   for STM32F407: USART_4 and USART_5 are UARTs
  * @note   max string length: 100 byte
  * @retval none
  */
void API_USART_send_string(API_USART_type_t* USART, const char* FormatString, ...)
{
    va_list  args;
    int32_t  nSize = 0;
    uint8_t  i = 0;
    char     SendString[100];

    /* format string */
    va_start(args, FormatString);
    nSize = vsnprintf(SendString, 100, FormatString, args);
    va_end(args);

    USART->busy = 1;

    /* send string */
    for(i = 0; i < nSize; i++)
    {
      USART_SendData(USART->USARTx, SendString[i]);
      while(USART_GetFlagStatus(USART->USARTx, USART_FLAG_TC) == RESET);
    }

    USART->busy = 0;
}

/**
  * @brief  Send a data using USART_x
  * @param  USART: can be USART_1..USART_6
  * @param  *ptr: Pointer to Data to send
  * @param  len:  Number of bytes to send
  * @note
  * @retval none
  */
void API_USART_send_data(API_USART_type_t* USART, const char *ptr, long len)
{
    long i;

    USART->busy = 1;

    for(i=0; i<len; i++)
    {
        API_USART_send_byte(USART, ptr[i]);
    }

    USART->busy = 0;
}

void API_USART_DMA_send(API_USART_type_t* USART, uint16_t tx_length)
{
    while (DMA_GetCurrDataCounter(USART->DMAx_Streamx_Tx) > 0);

    USART->busy = 1;

    DMA_Cmd(USART->DMAx_Streamx_Tx, DISABLE);
    DMA_SetCurrDataCounter(USART->DMAx_Streamx_Tx, tx_length);
    DMA_Cmd(USART->DMAx_Streamx_Tx, ENABLE);
}

void API_USART_DMA_receive(API_USART_type_t* USART, uint16_t rx_length)
{
  while (DMA_GetCurrDataCounter(USART->DMAx_Streamx_Rx) > 0);

  DMA_Cmd(USART->DMAx_Streamx_Rx, DISABLE);
  DMA_SetCurrDataCounter(USART->DMAx_Streamx_Rx, rx_length);
  DMA_Cmd(USART->DMAx_Streamx_Rx, ENABLE);
}

/**
  * @brief  Get a single Byte from USART_x buffer
  * @param  USART: can be USART_1..USART_6
  * @note   for STM32F407: USART_4 and USART_5 are UARTs
  * @note   call function USARTStartReceiving() before reading data from the buffer
  * @retval ReceivedByte: most recent received byte by the USARTx peripheral
  */
uint8_t API_USART_get_byte_from_buffer(API_USART_type_t* USART)
{
    uint8_t ReceivedByte = 0;

    ReceivedByte = (uint8_t)USART_ReceiveData(USART->USARTx);

    return ReceivedByte;
}

#endif // __USING_USART
