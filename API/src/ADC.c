/**
 * @file    ./API/src/ADC.h
 * @author  Andreas Hirtenlehner, Gerald Ebmer
 * @brief   API for ADC
 */

#include "PerUsings.h"
#ifdef __USING_ADC

/* Includes ------------------------------------------------------------------*/
#include "ADC.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
API_ADC_type_t API_ADC1 = { ADC1, RCC_APB2Periph_ADC1 };
API_ADC_type_t API_ADC2 = { ADC2, RCC_APB2Periph_ADC2 };
API_ADC_type_t API_ADC3 = { ADC3, RCC_APB2Periph_ADC3 };

double g_adc_ref_v = 2.5;

/* Private function prototypes -----------------------------------------------*/
void API_ADC_init(API_ADC_type_t* API_ADCx, API_GPIO_type_t* portpin);
double API_ADC_get_ue_v(API_ADC_type_t* API_ADCx);
double API_ADC_get_mcu_temp_gradc(void);

/* Private functions ---------------------------------------------------------*/

void API_ADC_init(API_ADC_type_t* API_ADCx, API_GPIO_type_t* portpin)
{
  ADC_InitTypeDef       ADC_InitStructure;
  ADC_CommonInitTypeDef ADC_CommonInitStructure;
  GPIO_InitTypeDef      GPIO_InitStructure;

  /* Clock enable */
  RCC_AHB1PeriphClockCmd(portpin->RCC_AHB1Periph_GPIOx, ENABLE);
  RCC_APB2PeriphClockCmd(API_ADCx->RCC_APB2Periph_ADCx, ENABLE);

  /* GPIO Init */
  GPIO_InitStructure.GPIO_Pin = portpin->GPIO_Pin_x;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
  GPIO_Init(portpin->GPIOx, &GPIO_InitStructure);

  /* ADC Common Init **********************************************************/
  ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
  ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
  ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
  ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
  ADC_CommonInit(&ADC_CommonInitStructure);

  /* ADC Init ****************************************************************/
  ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
  ADC_InitStructure.ADC_ScanConvMode = DISABLE;
  ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
  ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
  ADC_InitStructure.ADC_ExternalTrigConv =  0;
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_NbrOfConversion = 1;
  ADC_Init(API_ADCx->ADCx, &ADC_InitStructure);

  /* ADC regular channel configuration *************************************/
  if(API_ADCx->ADCx == ADC1 || API_ADCx->ADCx == ADC2)
  {
    if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_0)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_0, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_1)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_1, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_2)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_2, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_3)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_3, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_4)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_4, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_5)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_5, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_6)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_6, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_7)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_7, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOB && portpin->GPIO_Pin_x == GPIO_Pin_0)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_8, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOB && portpin->GPIO_Pin_x == GPIO_Pin_1)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_9, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOC && portpin->GPIO_Pin_x == GPIO_Pin_0)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_10, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOC && portpin->GPIO_Pin_x == GPIO_Pin_1)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_11, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOC && portpin->GPIO_Pin_x == GPIO_Pin_2)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_12, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOC && portpin->GPIO_Pin_x == GPIO_Pin_3)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_13, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOC && portpin->GPIO_Pin_x == GPIO_Pin_4)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_14, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOC && portpin->GPIO_Pin_x == GPIO_Pin_5)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_15, 1, ADC_SampleTime_3Cycles);
    else
#ifdef USE_EXCEPTION
        Exception("ADC.c: portpin ist keinem ADC Channel zugewiesen.")
#endif
    ;
  }
  else if(API_ADCx->ADCx == ADC3)
  {
    if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_0)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_0, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_1)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_1, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_2)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_2, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOA && portpin->GPIO_Pin_x == GPIO_Pin_3)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_3, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOC && portpin->GPIO_Pin_x == GPIO_Pin_0)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_10, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOC && portpin->GPIO_Pin_x == GPIO_Pin_1)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_11, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOC && portpin->GPIO_Pin_x == GPIO_Pin_2)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_12, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOC && portpin->GPIO_Pin_x == GPIO_Pin_3)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_13, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOF && portpin->GPIO_Pin_x == GPIO_Pin_6)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_4, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOF && portpin->GPIO_Pin_x == GPIO_Pin_7)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_5, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOF && portpin->GPIO_Pin_x == GPIO_Pin_8)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_6, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOF && portpin->GPIO_Pin_x == GPIO_Pin_9)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_7, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOF && portpin->GPIO_Pin_x == GPIO_Pin_10)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_8, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOF && portpin->GPIO_Pin_x == GPIO_Pin_3)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_9, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOF && portpin->GPIO_Pin_x == GPIO_Pin_4)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_14, 1, ADC_SampleTime_3Cycles);
    else if(portpin->GPIOx == GPIOF && portpin->GPIO_Pin_x == GPIO_Pin_5)
        ADC_RegularChannelConfig(API_ADCx->ADCx, ADC_Channel_15, 1, ADC_SampleTime_3Cycles);
    else
#ifdef USE_EXCEPTION
        Exception("ADC.c: portpin ist keinem ADC Channel zugewiesen.")
#endif
        ;
  }
  else;

  /* Enable ADC */
  ADC_Cmd(API_ADCx->ADCx, ENABLE);
}

/**
  * @brief  Get a analog input voltage on a portpin.
  * @param  ADCx_OUT: where x can be (1,2,3) to select the ADC
  * @retval U: measured input Voltage
  */
double API_ADC_get_ue_v(API_ADC_type_t* API_ADCx)
{
  __IO uint16_t ConvertedValue = 0;

  /* Start Conversion */
  ADC_SoftwareStartConv(API_ADCx->ADCx);
  while(ADC_GetSoftwareStartConvStatus(API_ADCx->ADCx) == SET);
  ConvertedValue = ADC_GetConversionValue(API_ADCx->ADCx);

  return (g_adc_ref_v *  ConvertedValue / 4096.0);
}

/**
  * @brief  Get processor temperature
  * @param  none
  * @retval temperature in �C
  */
double API_ADC_get_mcu_temp_gradc()
{
  ADC_InitTypeDef       ADC_InitStructure;
  ADC_CommonInitTypeDef ADC_CommonInitStructure;
  __IO uint16_t ConvertedValue = 0;
  double MCUtemp = 0.0;

  /* Clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

  /* ADC Common Init **********************************************************/
  ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
  ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
  ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
  ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
  ADC_CommonInit(&ADC_CommonInitStructure);

  /* ADC Init ****************************************************************/
  ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
  ADC_InitStructure.ADC_ScanConvMode = DISABLE;
  ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
  ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
  ADC_InitStructure.ADC_ExternalTrigConv =  0;
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_NbrOfConversion = 1;
  ADC_Init(ADC1, &ADC_InitStructure);

  /* ADC regular channel configuration *************************************/

  // ADC1 Configuration, ADC_Channel_TempSensor is actual channel 16
  ADC_RegularChannelConfig(ADC1, ADC_Channel_TempSensor, 1, ADC_SampleTime_144Cycles);

  // Enable internal temperature sensor
  ADC_TempSensorVrefintCmd(ENABLE);

  /* Enable ADC */
  ADC_Cmd(ADC1, ENABLE);

  /* Start Conversion */
  ADC_SoftwareStartConv(ADC1);
  while(ADC_GetSoftwareStartConvStatus(ADC1) == SET);
  ConvertedValue = ADC_GetConversionValue(ADC1);

  MCUtemp = g_adc_ref_v *  ConvertedValue / 4096.0;
  MCUtemp -= 0.760;
  MCUtemp /= 0.0025;
  MCUtemp += 25.0;

  return MCUtemp;
}

#endif // __USING_ADC
