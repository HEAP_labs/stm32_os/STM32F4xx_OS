/**
  ******************************************************************************
  * @file    ./API/inc/DAC.h 
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   Header for DAC module
  */
  
#ifndef __DAC_H
#define __DAC_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "GPIO.h"
#include "Timer.h"

/* Exported types ------------------------------------------------------------*/
typedef struct{
	__IO uint32_t DAC_Channel_x;
	API_GPIO_type_t* portpin;
	API_TIM_type_t* API_TIMx;
	uint32_t	DMA_Channel_x;
	DMA_Stream_TypeDef* DMA_Stream_x;
	uint32_t  DAC_Address;
	uint32_t  TriggerSource;
} API_DAC_type_t;

extern API_DAC_type_t API_DAC1;
extern API_DAC_type_t API_DAC2;

extern double g_dac_ref_v;

/* Exported constants --------------------------------------------------------*/

#define DAC_DHR12R1_ADDRESS    0x40007408
#define DAC_DHR12L1_ADDRESS    0x4000740C
#define DAC_DHR8R1_ADDRESS     0x40007410 
#define DAC_DHR12R2_ADDRESS    0x40007414
#define DAC_DHR12L2_ADDRESS    0x40007418
#define DAC_DHR8R2_ADDRESS     0x4000741C
#define DAC_DHR12RD_ADDRESS    0x40007420
#define DAC_DHR12LD_ADDRESS    0x40007424
#define DAC_DHR8RD_ADDRESS     0x40007428

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void API_DAC_set_ua_v(API_DAC_type_t* API_DACx, double u_v);
extern void API_DAC_DMA_set_ua_v(API_DAC_type_t* API_DACx, uint32_t *u_digit, double t_period_s, uint32_t length);

#endif
