/**
  * @file    ./API/inc/SPI.h
  * @author  Andreas Hirtenlehner
  * @brief   Header for SPI module
  */

#ifndef __SPI_H
#define __SPI_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "GPIO.h"
#include "errno.h"

/* Exported types ------------------------------------------------------------*/
typedef struct{
  uint8_t              is_init;
  SPI_TypeDef*         SPIx;
  uint32_t             RCC_APBxPeriph_SPIx;
  uint8_t              GPIO_AF_SPIx;
  API_GPIO_type_t*     mosi_portpin;
  API_GPIO_type_t*     miso_portpin;
  API_GPIO_type_t*     sck_portpin;
  API_GPIO_type_t*     nss_portpin;
  DMA_TypeDef*         DMAx;
  uint32_t             DMA_Channel_x;
  DMA_Stream_TypeDef*  DMAx_Streamx_Tx;
  DMA_Stream_TypeDef*  DMAx_Streamx_Rx;
  IRQn_Type            DMA_Tx_IRQn;
  IRQn_Type            DMA_Rx_IRQn;
  uint32_t             RCC_AHB1Periph_DMAx;
  uint8_t*             p_tx_data;
  uint8_t*             p_rx_data;
} API_SPI_type_t;

extern API_SPI_type_t API_SPI1;
extern API_SPI_type_t API_SPI2;
extern API_SPI_type_t API_SPI3;

/* Exported constants --------------------------------------------------------*/
#define SPI_MODE_0 0x01 // CPOL=0, CPHA=0
#define SPI_MODE_1 0x02 // CPOL=0, CPHA=1
#define SPI_MODE_2 0x04 // CPOL=1, CPHA=0
#define SPI_MODE_3 0x08 // CPOL=1, CPHA=1

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void API_SPI_init(API_SPI_type_t* API_SPIx, API_GPIO_type_t* mosi_portpin, API_GPIO_type_t* miso_portpin, API_GPIO_type_t* sck_portpin, API_GPIO_type_t* nss_portpin, uint16_t baudrate_prescaler, uint8_t mode);
extern void API_SPI_DMA_init(API_SPI_type_t* API_SPIx, uint8_t* p_tx_data, uint8_t* p_rx_data);
extern void API_SPI_DMA_send(API_SPI_type_t* API_SPIx, uint16_t tx_length);
extern void API_SPI_DMA_send_and_receive(API_SPI_type_t* API_SPIx, uint16_t length);
extern void API_SPI_send_byte(API_SPI_type_t* API_SPIx, uint8_t data);
extern uint8_t API_SPI_receive_byte(API_SPI_type_t* API_SPIx);
extern error_t API_SPI_send_then_receive_statemachine(API_SPI_type_t* API_SPIx, double t_period_s, uint8_t *p_tx_data, uint8_t tx_length, uint8_t *p_rx_data, uint8_t rx_length);
extern error_t API_SPI_DMA_send_then_receive_statemachine(API_SPI_type_t* API_SPIx, double t_period_s, uint8_t* p_tx_data, uint8_t  tx_length, uint8_t* p_rx_data, uint8_t  rx_length, uint8_t* p_tx_data_while_receiving); 

#endif
