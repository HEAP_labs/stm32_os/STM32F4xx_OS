/**
  * @file    ./API/inc/ADC.h 
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   Header for ADC module
  */

#ifndef __ADC_H
#define __ADC_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "GPIO.h"

/* Exported types ------------------------------------------------------------*/
typedef struct{
	ADC_TypeDef*  ADCx;
	__IO uint32_t RCC_APB2Periph_ADCx;
} API_ADC_type_t;

extern API_ADC_type_t API_ADC1;
extern API_ADC_type_t API_ADC2;
extern API_ADC_type_t API_ADC3;

extern double g_adc_ref_v;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void API_ADC_init(API_ADC_type_t* API_ADCx, API_GPIO_type_t* portpin);
extern double API_ADC_get_ue_v(API_ADC_type_t* API_ADCx);
extern double API_ADC_get_mcu_temp_gradc(void);

#endif // __USING_ADC
