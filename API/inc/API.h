/**
  ******************************************************************************
  * @file    ./API/inc/API.h 
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   Header to implement API
  */

#ifndef __API_H
#define __API_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "PerUsings.h"

#ifdef __USING_GPIO
  #include "GPIO.h"	  
#endif // __USING_GPIO

#ifdef __USING_TIMER
  #include "Timer.h"	  
#endif // __USING_TIMER

#ifdef __USING_ADC
  #include "ADC.h"	  
#endif // __USING_ADC

#ifdef __USING_DAC
  #include "DAC.h"	  
#endif // __USING_DAC

#ifdef __USING_USART
  #include "USART.h"	  
#endif // __USING_USART

#ifdef __USING_SPI
  #include "SPI.h"	  
#endif // __USING_SPI

#ifdef __USING_WWDG
  #include "Watchdog.h"	  
#endif // __USING_WWDG

#ifdef __USING_IWDG
  #include "Watchdog.h"	  
#endif // __USING_IWDG

#ifdef __USING_RTC
  #include "RTC.h"	  
#endif // __USING_RTC

#ifdef __USING_FLASH
  #include "Flash.h"	  
#endif // __USING_FLASH

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif
