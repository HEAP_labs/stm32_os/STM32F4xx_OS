/**
  * @file    ./API/inc/Timer.h
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   Header for Timer module
  */

#ifndef __TIMER_H
#define __TIMER_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "GPIO.h"

/* Exported types ------------------------------------------------------------*/
typedef struct{
  TIM_TypeDef*  TIMx;
  __IO uint32_t RCC_APBxPeriph_TIMx;
  __IO uint8_t  GPIO_AF_TIMx;
  IRQn_Type     IRQnx;
  uint32_t      period_timer_ticks;
} API_TIM_type_t;

/* Exported constants --------------------------------------------------------*/
#define CH1 0x01
#define CH2 0x02
#define CH3 0x04
#define CH4 0x08

extern API_TIM_type_t API_TIM1;
extern API_TIM_type_t API_TIM2;
extern API_TIM_type_t API_TIM3;
extern API_TIM_type_t API_TIM4;
extern API_TIM_type_t API_TIM5;
extern API_TIM_type_t API_TIM6;
extern API_TIM_type_t API_TIM7;
extern API_TIM_type_t API_TIM8;
extern API_TIM_type_t API_TIM9;
extern API_TIM_type_t API_TIM1;
extern API_TIM_type_t API_TIM11;
extern API_TIM_type_t API_TIM12;
extern API_TIM_type_t API_TIM13;
extern API_TIM_type_t API_TIM14;

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void API_start_taskclass(API_TIM_type_t* API_TIMx, double t_period_s, uint8_t priority);
extern void API_PWM_start(API_TIM_type_t* API_TIMx, uint32_t CHx, API_GPIO_type_t* portpin, double t_period_s, double dutycycle, uint16_t TIM_OCPolarity);
extern void API_PWM_start_complementary(API_TIM_type_t* API_TIMx, API_GPIO_type_t* portpin, double t_period_s, double dutycycle, uint8_t DeadTime_ClkCycles, uint16_t TIM_OCPolarity);
extern void API_PWM_set_dutycycle(API_TIM_type_t* API_TIMx, uint8_t CHx, double dutycycle);
  
#endif // __TIMER_H
