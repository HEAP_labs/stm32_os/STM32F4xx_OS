/**
  ******************************************************************************
  * @file    ./API/inc/PerUsings.h
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   peripherial usage definitions
  */

#ifndef __PERUSINGS_H
#define __PERUSINGS_H

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define __USING_GPIO
#define __USING_TIMER
//#define __USING_ADC
//#define __USING_DAC
//#define __USING_USART
//#define __USING_SPI
//#define __USING_WWDG
//#define __USING_IWDG
//#define __USING_RTC
//#define __USING_FLASH

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif // __PERUSINGS_H
